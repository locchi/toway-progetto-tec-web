<div class="immagine-evento">
    <img src="img/<?php echo $templateParams["imm"]; ?>" alt="" />
</div><div class="informazioni-evento">
    <div class="event-title">
        <?php echo $templateParams["titolo"]; ?>
    </div><div class="event-description">
        <?php echo $templateParams["desc"]; ?>
    </div>
</div><div class="biglietti-evento">
    <h2>Biglietti</h2>
    <table class="event-table">
        <tr>
            <th>Evento </th>
            <th>Localit&agrave </th>
            <th>Data </th>
            <th class="ora-evento">Inizio </th>
            <th>Prezzo </th>
            <?php if(isset($templateParams["tic"])) : ?>
            <th>Biglietti acquistati</th>
            <?php else : ?>
            <th>Acquista</th>
            <?php endif; ?>
        </tr>
        <?php $x=0;
        $dateT = date_create(date("Y-m-d"));
        $dataI = explode("-", $templateParams["dataIn"]);
        $anno = (int)$dataI[0];
        $mese = (int)$dataI[1];
        $giorno = (int)$dataI[2];
        while($x <= $templateParams["tot"]) : ?>
        <?php if(isset($templateParams["tic"])) : ?>
        <tr class="event-tr">
            <td class="name-evento"><?php echo $templateParams["titolo"]; ?></td>
            <td class="localita-evento"><?php echo $templateParams["localita"]; ?></td>
            <td><?php echo $giorno."/".$mese."/".$anno; ?></td>
            <td class="ora-evento"><?php echo $templateParams["ora_inizio"]; ?></td>
            <td class="biglietti"><?php if((count($dbh->getOccupiedSeats($templateParams["id"], ($anno."-".$mese."-".$giorno))) < ($dbh->getPosti($templateParams["localita"]))[0]["posti"])): ?>
                <img src="./img/green.png" alt="" />Biglietti a <?php echo $templateParams["prezzo"]; ?> €<?php else : ?><img src="./img/red.png" alt="" />Biglietti terminati<?php endif; ?></td>
            <td><?php echo count($dbh->getOccupiedSeats($templateParams["id"],$anno."-".$mese."-".$giorno));?><?php if(!(count($dbh->getOccupiedSeats($templateParams["id"], ($anno."-".$mese."-".$giorno))) < ($dbh->getPosti($templateParams["localita"]))[0]["posti"])): ?><span class="badgeT">!</span><?php endif; ?></td>
        </tr>
        <?php else : ?>
        <?php $daten = date_create($anno."-".$mese."-".$giorno); $res = date_diff($daten,$dateT); if(($res->format("%R%d")<=0 && $res->format("%R%y")<=0 && $res->format("%R%m")<=0)) : ?>
        <tr class="event-tr">
            <td class="name-evento"><?php echo $templateParams["titolo"]; ?></td>
            <td class="localita-evento"><?php echo $templateParams["localita"]; ?></td>
            <td><?php echo $giorno."/".$mese."/".$anno; ?></td>
            <td class="ora-evento"><?php echo $templateParams["ora_inizio"]; ?></td>
            <td class="biglietti"><?php if((count($dbh->getOccupiedSeats($templateParams["id"], ($anno."-".$mese."-".$giorno))) < ($dbh->getPosti($templateParams["localita"]))[0]["posti"])): ?>
                <img src="./img/green.png" alt="" />Biglietti a <?php echo $templateParams["prezzo"]; ?> €<?php else : ?><img src="./img/red.png" alt="" />Biglietti terminati<?php $fin=1; endif; ?></td>
        <td><a <?php $daten = date_create($anno."-".$mese."-".$giorno); $res = date_diff($daten,$dateT); if($res->format("%R%d")<=0 && $res->format("%R%y")<=0 && $res->format("%R%m")<=0 && !isset($fin)): ?>
        href=<?php echo "scelta-biglietti.php?id=".$templateParams["id"]."&date=".$anno."-".$mese."-".$giorno ?><?php elseif(!isset($fin)) : ?>class="period-expired"<?php else: ?> class="sold-out" <?php unset($fin); endif; ?>>Biglietti</a></td>
        </tr>
        <?php endif; ?>
        <?php endif; ?>
        <?php $giorno = $giorno + 1;
            if($giorno > $templateParams["dayMonth"]){
                $giorno = $giorno - $templateParams["dayMonth"];
                $mese = $mese + 1;
                if($mese > 12){
                    $anno = $anno + 1;
                    $mese = 1;
                }
            }
            $x=$x+1;
        endwhile; ?>
    </table>
</div>