<?php 
if(isset($templateParams['error'])) { 
   echo "<p class='city-error'>Errore nell'aggiunta di una nuova citt&agrave</p>";
}
?>


<div class="form-process">
   <h2>Aggiungi una nuova citt&agrave</h2>
   <form action="process-city.php" method="post" name="city_form">
      <div class="process-name">
            <div>Citt&agrave </div>
            <div><label for="city" hidden>Inserire la nuova citta'</label>
            <input type="text" name="city" id="city"/></div>
      </div>
      <div class="process-desc">
            <div>Tipo piantina </div>
            <div><label for="piant" hidden>Inserire il numero della piantina corrispondente</label>
            <input type="number" name="piant" id="piant"/></div>
      </div>
      <div class="process-btn">
            <label for="submit" hidden>Cliccare qui per aggiungere la nuova citta'</label>
            <input type="submit" value="Aggiungi"  id="submit"/>
      </div>
   </form>
</div>

<button class="log"><a href="login.php">Indietro</a></button>
