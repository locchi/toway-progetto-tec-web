<!DOCTYPE html>
<html lang="it">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title><?php echo $templateParams["titolo"]; ?></title>
    <link rel="shortcut icon" href="img/logo.png" />
    <link rel="stylesheet" type="text/css" href="./css/style.css" />
    <link rel="stylesheet" type="text/css" href="./css/style-evento.css" />
    <link rel="stylesheet" type="text/css" href="./css/style-biglietto.css" />
    <link rel="stylesheet" type="text/css" href="./css/style-mappa-evento.css" />
    <link rel="stylesheet" type="text/css" href="./css/style-carrello.css" />
    <link rel="stylesheet" type="text/css" href="./css/style-user.css" />
    <script src="./js/jquery-1.11.3.min.js" type="text/javascript"></script>
	<script src="./js/cookies-policy.js" type="text/javascript"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
    <script src="./js/base.js" type="text/javascript"></script>
    <script src="./js/ricerca.js" type="text/javascript"></script>
    <?php
    if(isset($templateParams["js"])):
    ?>
        <script src="./js/<?php echo $templateParams["js"]; ?>" type="text/javascript"></script>
    <?php
    endif;
    ?>
</head>
<body>
    <header>
        <img class="logo-sito" src="img/Toway-Logo.png" alt="logo del sito" />
        <div class="menu-icona">
            <img src="img/menu.png" alt="Apri il menu per visualizzarne le opzioni" />
        </div>
        <div class="login <?php if(isset($templateParams["userId"]) || isset($_COOKIE["userId"]) ) : ?>logged<?php endif; ?>"> <!-- Aggiunta classe logged -->
            <div class="login-logged">Benvenuto, <?php if(isset($templateParams["username"])) echo $templateParams["username"]; ?> <?php if(isset($_COOKIE["userId"]) && !(isset($templateParams["userId"])))  echo $dbh->getUserName($_COOKIE["userId"]);  ?>
            </div><div class="login-not-logged">
                <a href="login.php">Accedi o Registrati</a> <!-- Benvenuto, Luca Bazzocchi -->
            </div>
        </div>
    </header>
    <div class="menu">
        <nav class="menu-navigatore">
            <ul>
                <li><a href="login.php"><?php if(isset($templateParams["userId"]) || isset($_COOKIE["userId"]) ) : ?>Area Utente<?php else : ?>Accedi o Registrati<?php endif; ?></a> 
                </li><div class="dropdown dr-categorie">
                    <button class="dropbtn">Categorie</button>
                    <div class="dropdown-content">
                        <a href="eventiCercati.php?Concerti=1">Concerti</a>
                        <a href="eventiCercati.php?Teatro=1">Teatro</a>
                        <a href="eventiCercati.php?Cinema=1">Cinema</a>
                        <a href="eventiCercati.php?Sport=1">Sport</a>
                    </div>
                </div><div class="dropdown dr-luoghi">
                    <button class="dropbtn">Luoghi</button>
                    <div class="dropdown-content">
                        <a href="eventiCercati.php?city=Milano">Milano</a>
                        <a href="eventiCercati.php?city=Roma">Roma</a>
                        <a href="eventiCercati.php?city=Bologna">Bologna</a>
                        <a href="eventiCercati.php?city=Cesena">Cesena</a>
                        <a href="eventiCercati.php?city=Genova">Genova</a>
                        <a href="eventiCercati.php?city=Torino">Torino</a>
                    </div>
                </div><?php
                    if(isset($templateParams["ricercaAvanzata"])){
                        echo '<li class="ricerca"><span>Ricerca Avanzata</span></li>';
                    }
                    ?>
            </ul>
        </nav>
    </div>
    <div id="cookies-policy" class="cookies-policy" hidden="true">
        <p id="seen">Questo sito utilizza cookies tecnici per migliorare l'esperienza dell'utente</p>
        <a href="#" id="accept">OK</a>
        <a href="info.php" id="reject">Leggi l'informativa</a>
    </div>
    <nav class="navigatore">
        <ul>
            <li class="pul-carrello"><a href="pagina-carrello.php" class="carrello"><img src="img/carrello-bianco.png" alt="Link per il proprio carrello per visualizzare i biglietti attualmente in esso" /><p>Carrello
            </p><span class="badge" value="<?php if(isset($_COOKIE["userId"])){
                    echo count($dbh->getUnacquistedTickets($_COOKIE["userId"]));
                }else { echo 0;}?>">0</span></a>
            </li><div class="dropdown">
                    <button class="dropbtn">Categorie</button>
                    <div class="dropdown-content">
                        <a href="eventiCercati.php?Concerti=1">Concerti</a>
                        <a href="eventiCercati.php?Teatro=1">Teatro</a>
                        <a href="eventiCercati.php?Cinema=1">Cinema</a>
                        <a href="eventiCercati.php?Sport=1">Sport</a>
                    </div>
                </div><div class="dropdown">
                    <button class="dropbtn">Luoghi</button>
                    <div class="dropdown-content">
                        <a href="eventiCercati.php?city=Milano">Milano</a>
                        <a href="eventiCercati.php?city=Roma">Roma</a>
                        <a href="eventiCercati.php?city=Bologna">Bologna</a>
                        <a href="eventiCercati.php?city=Cesena">Cesena</a>
                        <a href="eventiCercati.php?city=Genova">Genova</a>
                        <a href="eventiCercati.php?city=Torino">Torino</a>
                    </div>
                </div><li class="barra-ricerca"><label for="text-ricerca" hidden>Testo usato per filtrare tra i nome degli eventi</label><input type="text" id="text-ricerca" name="text" placeholder="Evento da cercare" /><a href="eventiCercati.php" class="cerca" id="btn-ricerca"><img src="img/lente.png" alt="Effettua la ricerca usando i parametri inseriti" /><p>Cerca</p></a>
            </li>
        </ul>
    </nav><?php
    if(isset($templateParams["bestEvents"])){
        require($templateParams["bestEvents"]);
    }
    ?><div class="main-aside-container"><?php
        if(isset($templateParams["ricercaAvanzata"])){
            require($templateParams["ricercaAvanzata"]);
        }
        ?><main id="main"><?php
            if(isset($templateParams["listaeventi"])){
                require($templateParams["listaeventi"]);
            }
            elseif(isset($templateParams["evento"])){
                require($templateParams["evento"]);
            }
            elseif(isset($templateParams["sceltaposti"])){
                require($templateParams["sceltaposti"]);
            }
        ?>
        </main>
    </div><footer>
        <p>Progetto Tecnologie Web - A.A. 2019/2020</p>
        <p>Autori: Bazzocchi Luca, Casadei Giacomo</p>
        <p><a href="info.php">Informativa Cookies</a></p>
    </footer>
</body>
</html>