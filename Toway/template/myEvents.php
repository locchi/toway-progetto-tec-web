<?php if(isset($templateParams["gestor"])) : ?>
<h2 class="log">Eventi organizzati da me</h2>
<ul class="log gestor"> 
<?php foreach($templateParams["gestor"] as $eve) : ?>
<li><img src=<?php echo IMG_DIR.$eve["immagine"]; ?> alt="immagine evento" height="100" width="100"> <p><?php echo $eve["nome"]; ?>  <br/>
Dal <?php echo changeDate($eve["data_inizio"]); ?> al <?php echo changeDate($eve["data_fine"]); ?> </p><button class="log gestevent"><a href="alterEvent.php?eid=<?php echo $eve["id"]; ?>">Modifica</a></button><button class="log bigliett"><a href="pagina-evento.php?id=<?php echo $eve["id"]; ?>&tic=1">Controlla i biglietti</a></button></li>
<?php endforeach; ?>
</ul>
<?php endif; ?>

<?php if(isset($templateParams["client"])) : ?>
<h2 class="log">I miei eventi</h2>
<ul class="log client"> 
<?php $visti = array("x"); ?>
<?php foreach($templateParams["client"] as $eve) : ?>
<?php if(!(array_search($eve["id"],$visti)>0)) : ?>
<?php array_push($visti, $eve["id"]); ?>
<li><img src=<?php echo IMG_DIR.$eve["immagine"]; ?> alt="immagine evento"><p><?php echo $eve["nome"]; ?>  <br/>
Il <?php echo changeDate($eve["data_evento"]); ?> posto/i <?php foreach($templateParams[$eve["id"]] as $post) echo $post." "; ?> </p>
<?php if($eve["changed"]==1) : ?> <div class="notification"><span class="badge-ev">!</span></div><button class="log event"><a href="pagina-evento.php?id=<?php echo $eve["id_evento"]; ?>&mod=1"> Pagina Evento</a></button> <?php else : ?>  <button class="log event"><a href="pagina-evento.php?id=<?php echo $eve["id_evento"]; ?>&mod=1"> Pagina Evento</a></button> <?php endif; ?></li>
<?php endif; ?>
<?php endforeach; ?>
</ul>
<?php endif; ?>

<button class="log"><a href="login.php">Indietro</a></button>


