<div class="immagine-evento">
    <img src=<?php echo IMG_DIR.$templateParams["event"][0]["immagine"]; ?> alt="immagine evento" />
</div><div class="informazioni-biglietto">
    <div class="informazioni-nome"><?php echo $templateParams["event"][0]["nome"]; ?></div>
    <div class="informazioni-data">Nel giorno <?php echo changeDate($templateParams["date"]); ?></div>
    <div class="informazioni-orario">Ore <?php echo $templateParams["event"][0]["ora_inizio"]; ?></div>
    <div class="informazioni-localita">A <?php echo $templateParams["event"][0]["citta"]; ?></div>
    <div class="informazioni-via"><?php echo $templateParams["event"][0]["via"]; ?></div>
</div><div class="eid" hidden value=<?php echo $templateParams["event"][0]["id"]; ?>>
</div><div class="date" hidden value=<?php echo $templateParams["date"]; ?>>
</div><div class="biglietti-evento">
    <h2>Biglietti</h2>
    <div class="scelta-biglietti">
        <div class="scelta-migliorposto">
            <input type="button" value="Scelta Miglior Posto" class="btn-scelta-mig">
        </div><div class="scelta-mappa">
            <input type="button" value="Scelta In Mappa" class="btn-scelta-map">
        </div>
        <div class="scelta-selezionata">
            <div class="migliorposto">
                <table>
                    <tr>
                        <td>Intero</td>
                        <td class="localita-evento"><p>Prezzo</p> <p class="prezzo-biglietto"><?php echo $templateParams["event"][0]["prezzo"]; ?></p> <p>€</p></td>
                        <td>Quantitativo
                            <label for="hall" hidden>Selezionare il numero di biglietti desiderati</label>
                            <select name="hall" id="hall">
                                <option selected="selected" value="0">0</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                            </select>
                        </td>
                    </tr>
                </table>
</div><div class="mappa">
                <?php
                    if(isset($templateParams["mappa"])){
                        require($templateParams["mappa"]);
                    }
                ?><div class="biglietti-selezionati">
                    <p>Biglietti Selezionati<br/>Nessuno</p>
                    <div>
                    </div>
                </div>
            </div><form class="aggiunta-carrello"><a>Aggiungi al Carrello</a></form>
        </div>
    </div>
</div>