<script type="text/javascript" src="./js/sha512.js"></script>
<script type="text/javascript" src="./js/form.js"></script>
<?php
if(isset($templateParams['error'])) { 
   echo '<p class="err">Email e/o password non corretta</p>';
}
if(isset($templateParams["register"])) {
   echo '<p class="no-err">Registrazione completata con successo</p>';
}
if(isset($templateParams["errorMail"])) {
   echo '<p class="err">Esiste gi&agrave un account associato a questa email</p>';
}
if(isset($templateParams["errorparam"])) {
   echo '<p class="err">Alcuni campi non sono validi</p>';
}
?>

<?php if(!(isset($_COOKIE["userId"]) || isset($templateParams["userId"]))): ?>
   <div class="formlog">
      <div class="formlog-login">
         <h2 class="log">Accedi</h2>
         <form class="log" action="process_login.php" method="post" name="login_form">
            <div class="log-mail">
                <div>Email </div>
                <div><label for="email" hidden>Inserire l'email per effettuare il login</label>
                <input type="text" name="email" id="email"/></div>
            </div>
            <div class="log-psw">
                <div>Password </div>
                <div><label for="password" hidden>Inserire la password per effettuare il login</label>
                <input type="password" name="pas" id="password"/></div>
            </div>
            <div class="log-btn">
            <label for="login" hidden>Cliccare per effettuare il login</label>
               <input type="button" value="Login" id="login" onclick="formhash(this.form, this.form.password);" />
            </div>
         </form>
      </div><div class="formlog-register">
         <h2 class="log">Registrati</h2>
         <form class="log" action="process-register.php" method="post" name="register_form">
            <div class="reg-usr">
                <div>Username </div>
                <div><label for="username" hidden>Inserire lo username per effettuare la registrazione</label>
                <input type="text" name="username" id="username"/></div>
            </div>
            <div class="reg-mail">
                <div>Email </div>
                <div>
                <label for="emailr" hidden>Inserire l'email per effettuare la registrazione</label>
                <input type="text" name="email" id="emailr"/></div>
            </div>
            <div class="reg-psw">
                <div>Password </div>
                <div><label for="passwordr" hidden>Inserire la password per effettuare la registrazione</label>
                <input type="password" name="pas" id="passwordr"/></div>
            </div>
            <div class="reg-type">
                <div>Tipo </div>
                <label for="type" hidden>Selezionare il tipo di utente</label>
                <div><select name="type" id="type">
                     <option value="client">Client</option>
                     <option value="gestore">Gestore</option>
                     <option value="admin">Admin</option>
               </select></div>
            </div>
            <div class="reg-btn">
            <label for="register" hidden>Cliccare per effettuare registrazione</label>
               <input type="button" value="Registrati" id="register" onclick="formhash(this.form, this.form.passwordr);" />
            </div>
         </form>
      </div>
   </div>
<?php endif; ?>

<?php if(isset($_COOKIE["userId"]) && !(isset($templateParams["userId"]))) : ?>
<p class="log p">Username: <?php echo $dbh->getUserName($_COOKIE["userId"]); ?></p>
   <button class="log index"><a href="index.php"> Esplora gli eventi disponibili!</a></button>
   <button class="log logout"><a href="logout.php"> Logout</a></button>
   <?php $c = $dbh->getChangedById($_COOKIE["userId"]); if((count($c) > 0) && $c[0]["changed"] == 1) : ?>
      <button class="log myevents"><a href="mieiEventi.php"> I miei eventi <span class="badge-myEvents">!</span> </a></button>
   <?php else :?>
      <button class="log myevents"><a href="mieiEventi.php"> I miei eventi </a></button>
   <?php endif; ?>
<?php endif; ?>

<?php if(isset($templateParams["userId"])) : ?>
<p class="log p">Username: <?php echo $templateParams["username"]; ?></p>
   <button class="log index"><a href="index.php"> Esplora gli eventi disponibili!</a></button>
   <button class="log logout"><a href="logout.php"> Logout</a></button>
   <?php $c = $dbh->getChangedById($templateParams["userId"]); if((count($c) > 0) && $c[0]["changed"] == 1) : ?>
      <button class="log myevents"><a href="mieiEventi.php"> I miei eventi <span class="badge-myEvents">!</span> </a></button>
   <?php else :?>
      <button class="log myevents"><a href="mieiEventi.php"> I miei eventi </a></button>
   <?php endif; ?>
<?php endif; ?>

<?php if((isset($_COOKIE["userId"]) && ($dbh->getUserType($_COOKIE["userId"])=="gestore" || $dbh->getUserType($_COOKIE["userId"])=="admin")) || (isset($templateParams["userType"]) && ($templateParams["userType"]=="gestore" || $templateParams["userType"]=="admin")) ) :?>
   <button class="log newevent"><a href="creaEvento.php"> Organizza il tuo evento!</a></button>
   <button class="log eventsbyme"><a href="gestisciEventi.php"> Gestisci gli eventi organizzati da te!</a></button>
<?php endif; ?>

<?php if((isset($_COOKIE["userId"]) && $dbh->getUserType($_COOKIE["userId"])=="admin") || (isset($templateParams["userType"]) && $templateParams["userType"]=="admin") ) :?>
   <button class="log city"><a href="aggiungiCitta.php"> Inserisci una nuova citt&agrave tra quelle convenzionate!</a></button>
<?php endif; ?>



<?php if(isset($templateParams['insSuccess'])) 
echo "Evento creato con successo" ?>
