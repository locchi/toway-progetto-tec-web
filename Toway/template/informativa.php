<h2 class="cok">Informativa Estesa Cookies</h2>
<article class="informativa">
<p>Utilizziamo i cookie per rendere il nostro sito pi&ugrave facile ed intuitivo.</p>
<h3>Cosa sono i cookie?</h3>
    <p>I cookie sono piccoli file di testo inviati dal sito al terminale dell’interessato (solitamente al browser), dove vengono memorizzati per essere poi ritrasmessi al sito alla successiva visita del medesimo utente. Un cookie non pu&ograve richiamare nessun altro dato dal disco fisso dell’utente n&eacute trasmettere virus informatici o acquisire indirizzi email. Ogni cookie &egrave unico per il web browser dell’utente. Alcune delle funzioni dei cookie possono essere demandate ad altre tecnologie. Nel presente documento con il termine ‘cookie’ si vuol far riferimento sia ai cookie, propriamente detti, sia a tutte le tecnologie similari.</p>
    <h3>Tipologia dei cookie</h3>
    <p>I cookie possono essere di prima o di terza parte, dove per "prima parte" si intendono i cookie che riportano come dominio il sito, mentre per "terza parte" si intendono i cookie che sono relativi a domini esterni.</p>
    <p>I cookie di terza parte sono necessariamente installati da un soggetto esterno, sempre definito come "terza parte", non gestito dal sito. Tali soggetti possono eventualmente installare anche cookie di prima parte, salvando sul dominio del sito i propri cookie. Questo genere di cookie non &egrave usato su questo sito</p>
    <p>Un altro tipo di cookie sono i cosiddetti "Flash Cookie" (Local Shared Objects), utilizzati all'interno di Adobe Flash Player per erogare alcuni contenuti, come video clip o animazioni, in modo da ricordare le impostazioni e preferenze. I Flash cookie sono archiviati sul dispositivo, ma sono gestiti attraverso un’interfaccia differente rispetto a quella fornita dal browser utilizzato.</p>
    <h3>Natura dei cookie</h3>
    <p>Relativamente alla natura dei cookie, ne esistono di diversi tipi:</p>
    <p><strong>Cookie tecnici</strong><br>I cookie tecnici sono quelli utilizzati al solo fine di "effettuare la trasmissione di una comunicazione su una rete di comunicazione elettronica, o nella misura strettamente necessaria al fornitore di un servizio della societ&agrave dell'informazione esplicitamente richiesto dall'abbonato o dall'utente a erogare tale servizio" (cfr. art. 122, comma 1, del Codice).</p>
    <p>Essi non vengono utilizzati per scopi ulteriori e sono normalmente installati direttamente dal titolare o gestore del sito web. Possono essere suddivisi in:</p>
    <ul>
    <li>Cookie di navigazione o di sessione, che garantiscono la normale navigazione e fruizione del sito web (permettendo, ad esempio, di realizzare un acquisto o autenticarsi per accedere ad aree riservate); essi sono di fatto necessari per il corretto funzionamento del sito;</li>
    <li>Cookie analytics, assimilati ai cookie tecnici laddove utilizzati direttamente dal gestore del sito per raccogliere informazioni, in forma aggregata, sul numero degli utenti e su come questi visitano il sito stesso, al fine di migliorare le performance del sito;</li>
    <li>Cookie di funzionalit&agrave, che permettono all'utente la navigazione in funzione di una serie di criteri selezionati (ad esempio, la lingua, i prodotti selezionati per l'acquisto) al fine di migliorare il servizio reso allo stesso. Per l'installazione di tali cookie non &egrave richiesto il preventivo consenso degli utenti (pi&ugrave informazioni nel paragrafo Gestione dei cookie in basso).</li>
    </ul>
    <p><strong>Cookie di profilazione</strong><br>I cookie di profilazione sono volti a creare profili relativi all'utente e vengono utilizzati al fine di inviare messaggi pubblicitari in linea con le preferenze manifestate dallo stesso nell'ambito della navigazione in rete.</p>
    <p>Per l'utilizzo dei cookie di profilazione &egrave richiesto il consenso dell'interessato. Secondo il provvedimento (pi&ugrave informazioni nel paragrafo Gestione dei cookie in basso) l’utente pu&ograve autorizzare o negare il consenso all'installazione dei cookie attraverso le opzioni fornite nella sezione "Gestione dei cookie".</p>
    <p>In caso di cookie di terze parti, il sito non ha un controllo diretto dei singoli cookie e non pu&ograve controllarli (non pu&ograve n&eacute installarli direttamente n&eacute cancellarli). Puoi comunque gestire questi cookie attraverso le impostazioni del browser (segui le istruzioni riportate pi&ugrave avanti), o i siti indicati nella sezione "Gestione dei cookie".</p>
    <p>Questo tipo di cookie non &egrave utilizzato in questo sito</p>
    
    <h3>Durata dei cookie</h3>
    <p>I cookie hanno una durata dettata dalla data di scadenza (o da un'azione specifica come la chiusura del browser) impostata al momento dell'installazione.<br>I cookie possono essere:<br>• Temporanei o di sessione (session cookie): sono utilizzati per archiviare informazioni temporanee, consentono di collegare le azioni eseguite durante una sessione specifica e vengono rimossi dal computer alla chiusura del browser;<br>• Permanenti (persistent cookie): sono utilizzati per archiviare informazioni, ad esempio il nome e la password di accesso, in modo da evitare che l'utente debba digitarli nuovamente ogni volta che visita un sito specifico. Questi rimangono memorizzati nel computer anche dopo aver chiuso il browser.</p>
    <h3>Gestione dei cookie</h3>
    <p>In rispetto alle direttive del Codice in materia di protezione dei dati personali (d.lg. 30 giugno 2003, n. 196, di seguito "Codice") e, in particolare, agli artt. 13, comma 3 e 122, comma 1 e in riferimento al provvedimento di Individuazione delle modalit&agrave semplificate per l'informativa e l'acquisizione del consenso per l'uso dei cookie - 8 maggio 2014 (Pubblicato sulla Gazzetta Ufficiale n. 126 del 3 giugno 2014), puoi modificare in qualsiasi momento il consenso ai cookie.</p>
    <p>IKEA Italia Retail S.r.l. terr&agrave opportuna traccia del/dei consenso/i dell’Utente attraverso un apposito cookie tecnico, considerato dal Garante Privacy uno strumento “non particolarmente invasivo”. L’Utente pu&ograve negare il suo consenso e/o modificare in ogni momento le proprie opzioni relative all’uso dei cookie da parte del presente Sito Web, tramite accesso alla presente Informativa Privacy che &egrave “linkabile” da ogni pagina del presente Sito.</p>
    <p>L’Utente ha la facolt&agrave in qualsiasi momento di esercitare i diritti riconosciuti dall'art. 7 del D.Lgs. 196/2003 e dal Regolamento Europeo n.2016/679 ed, in particolare, tra gli altri, di ottenere copia dei dati trattati, il loro aggiornamento, la loro origine, la finalit&agrave e la modalit&agrave del trattamento, la loro rettifica o integrazione, la loro cancellazione, la trasformazione in forma anonima o il blocco per i trattamenti in violazione di legge e di opporsi per motivi legittimi al trattamento.</p>
    
    <p>Se &egrave gi&agrave stato dato il consenso ma si vogliono cambiare le autorizzazioni dei cookie, bisogna cancellarli attraverso il browser, come indicato sotto, perch&eacute altrimenti quelli gi&agrave installati non verranno rimossi. In particolare, si tenga presente che non &egrave possibile in alcun modo controllare i cookie di terze parti, quindi se &egrave gi&agrave stato dato precedentemente il consenso, &egrave necessario procedere alla cancellazione dei cookie attraverso il browser (sezione dedicata) oppure chiedendo l'opt-out direttamente alle terze parti.</p>
   
    <h3>Come disabilitare/cancellare i cookie mediante configurazione del browser</h3>
    <p><strong>Chrome</strong><br><strong>1.</strong> Eseguire il Browser Chrome;<br><strong>2.</strong> Fare click sul men&ugrave presente nella barra degli strumenti del browser a fianco della finestra di inserimento url per la navigazione;<br><strong>3.</strong> Selezionare <strong>Impostazioni;</strong><br><strong>4.</strong> Fare clic su <strong>Mostra Impostazioni Avanzate;</strong><br><strong>5.</strong> Nella sezione “Privacy” fare clic su bottone <strong>“Impostazioni contenuti“;</strong><br><strong>6.</strong> Nella sezione “Cookie” &egrave possibile modificare le seguenti impostazioni relative ai cookie:</p>
    <ul>
    <li>Consentire il salvataggio dei dati in locale.</li>
    <li>Modificare i dati locali solo fino alla chiusura del browser.</li>
    <li>Impedire ai siti di impostare i cookie.</li>
    <li>Bloccare i cookie di terze parti e i dati dei siti.</li>
    <li>Gestire le eccezioni per alcuni siti internet.</li>
    <li>Eliminare uno o tutti i cookie.</li>
    </ul>
    
    <p><strong>Mozilla Firefox</strong><br><strong>1.</strong> Eseguire il Browser Mozilla Firefox;<br><strong>2.</strong> Fare click sul men&ugrave presente nella barra degli strumenti del browser a fianco della finestra di inserimento url per la navigazione;<br><strong>3.</strong> Selezionare <strong>Opzioni;</strong><br><strong>4.</strong> Selezionare il pannello <strong>Privacy;</strong><br><strong>5.</strong> Fare clic su <strong>Mostra Impostazioni Avanzate;</strong><br><strong>6.</strong> Nella sezione “Privacy” fare clic su bottone <strong>“Impostazioni contenuti“;</strong><br><strong>7.</strong> Nella sezione <strong>“Tracciamento”</strong> &egrave possibile modificare le seguenti impostazioni relative ai cookie:<br>• Richiedi ai siti di non effettuare alcun tracciamento;<br>• Comunica ai siti la disponibilit&agrave ad essere tracciato;<br>• Non comunicare alcuna preferenza relativa al tracciamento dei dati personali.<br><strong>8.</strong> Dalla sezione “Cronologia” &egrave possibile:</p>
    <ul>
    <li>Abilitando “Utilizza impostazioni personalizzate” selezionare di accettare i cookie di terze parti (sempre, dai siti pi&ugrave visitato o mai) e di conservarli per un periodo determinato (fino alla loro scadenza, alla chiusura di Firefox o di chiedere ogni volta).</li>
    <li>Rimuovere i singoli cookie immagazzinati.</li>
    </ul>
    
    <p><strong>Internet Explorer</strong><br><strong>1.</strong> Eseguire il Browser Internet Explorer;<br><strong>2.</strong> Fare click sul pulsante <strong>Strumenti</strong> e scegliere <strong>Opzioni Internet;</strong><br><strong>3.</strong> Fare click sulla scheda <strong>Privacy</strong> e, nella sezione <strong>Impostazioni</strong>, modificare il dispositivo di scorrimento in funzione dell’azione desiderata per i cookie:</p>
    <ul>
    <li>Bloccare tutti i cookie.</li>
    <li>Consentire tutti i cookie.</li>
    <li>Selezionare i siti da cui ottenere cookie: spostare il cursore in una posizione intermedia in modo da non bloccare o consentire tutti i cookie, premere quindi su Siti, nella casella Indirizzo Sito Web inserire un sito internet e quindi premere su Blocca o Consenti.</li>
    </ul>
    
    <p><strong>Safari 6</strong><br><strong>1.</strong> Eseguire il Browser Safari;<br><strong>2.</strong> Fare click su <strong>Safari</strong>, selezionare <strong>Preferenze</strong> e premere su <strong>Privacy;</strong><br><strong>3.</strong> Nella sezione Blocca Cookie specificare come Safari deve accettare i cookie dai siti internet<strong>;</strong><br><strong>4.</strong> Per visionare quali siti hanno immagazzinato i cookie cliccare su <strong>Dettagli.</strong><br>Per maggiori informazioni visita la pagina dedicata.</p>
    <p><strong>Safari iOS (dispositivi mobile)</strong><br><strong>1.</strong> Eseguire il Browser Safari iOS;<br><strong>2.</strong> Tocca su <strong>Impostazioni</strong> e poi <strong>Safari</strong>;<br><strong>3.</strong> Tocca su <strong>Blocca Cookie</strong> e scegli tra le varie opzioni: “Mai”, “Di terze parti e inserzionisti” o “Sempre”;<br><strong>4.</strong> Per cancellare tutti i cookie immagazzinati da Safari, tocca su <strong>Impostazioni</strong>, poi su <strong>Safari</strong> e infine su <strong>Cancella Cookie e dati.</strong><br></p>
    <p><strong>Opera</strong><br><strong>1.</strong> Eseguire il Browser Opera;<br><strong>2.</strong> Fare click sul Preferenze poi su Avanzate e infine su Cookie;<br><strong>3.</strong> Selezionare una delle seguenti opzioni:</p>
    <ul>
    <li>Accetta tutti i cookie.</li>
    <li>Accetta i cookie solo dal sito che si visita: i cookie di terze parti e quelli che vengono inviati da un dominio diverso da quello che si sta visitando verranno rifiutati.</li>
    <li>Non accettare mai i cookie: tutti i cookie non verranno mai salvati.</li>
    </ul>
    <p>&nbsp;</p>
    <p class="inf-cook-last"><sup>Data ultimo aggiornamento: gennaio 2020.</sup></p>
    
    </article>