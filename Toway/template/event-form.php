<?php
if(isset($_GET['error'])) { 
   echo '<div class="process-error">Errore nella creazione dell evento. Parametri errati o mancanti.</div>';
}
?>


<div class="form-process">
   <h2>Creazione nuovo evento</h2>
   <form action="process-event.php" method="post" name="event_form" enctype="multipart/form-data">
      <div class="process-name">
            <div>Nome Evento </div>
            <div><label for="nome" hidden>Inserire il nome dell'evento</label>
            <input type="text" name="nome" id="nome" class="process-in" value="<?php if(isset($templateParams["eve"])) echo $templateParams["eve"][0]["nome"]; ?>"/></div>
      </div>
      <div class="process-desc">
            <div>Descrizione </div>
            <div><label for="desc" hidden>Inserire la descrizione dell'evento</label>
            <textarea type="text" name="desc" class="process-in" id="desc"><?php if(isset($templateParams["eve"])) echo $templateParams["eve"][0]["descrizione"]; ?></textarea></div>
      </div>
      <div class="process-price">
            <div>Prezzo in € </div>
            <div><label for="prezzo" hidden>Inserire il prezzo del biglietto per l'evento</label>
            <input type="number" name="prezzo" class="process-in" id="prezzo" value="<?php if(isset($templateParams["eve"])) echo $templateParams["eve"][0]["prezzo"]; ?>"/></div>
      </div>
      <div class="process-city">
            <div>Citt&agrave </div>
            <div><label for="citta" hidden>Selezionare la citta dove si svolgera' l'evento</label>
            <select name="citta" class="process-in" id="citta"><?php $c = $dbh->getCities(); foreach($c as $citta) : ?>
               <option value=<?php echo $citta["citta"]; ?> <?php if(isset($templateParams["eve"]) && ($templateParams["eve"][0]["citta"] == $citta["citta"])) echo "selected"; ?>><?php echo $citta["citta"]." - Posti: ".$citta["posti"]; ?></option>
               <?php endforeach; ?>
            </select></div>
      </div>
      <div class="process-street">
            <div>Via </div>
            <div><label for="via" hidden>Inserire la via dove si svolgera' l'evento</label>
            <input type="text" name="via" class="process-in" id="via" value="<?php if(isset($templateParams["eve"])) echo $templateParams["eve"][0]["via"]; ?>"/></div>
      </div>
      <div class="process-ds">
            <div>Data di inizio </div>
            <div><label for="start" hidden>Inserire la data di inizio dell'evento</label>
            <input type="date" name="start" class="process-in" id="start" value="<?php if(isset($templateParams["eve"])) echo $templateParams["eve"][0]["data_inizio"]; ?>"/></div>

      </div>
      <div class="process-de">
            <div>Data fine </div>
            <div><label for="end" hidden>Inserire la data di fine dell'evento</label>
            <input type="date" name="end" class="process-in" id="end" value="<?php if(isset($templateParams["eve"])) echo $templateParams["eve"][0]["data_fine"]; ?>"/></div>
      </div>
      <div class="process-street">
            <div>Ora di inizio </div>
            <div><label for="time" hidden>Inserire l'orario in cui inizia l'evento</label>
            <input type="time" name="time" id="time" value="<?php if(isset($templateParams["eve"])) echo $templateParams["eve"][0]["ora_inizio"]; ?>"/></div>
      </div>
      <div class="process-ds">
            <div>Categoria </div>
            <div><label for="cat" hidden>Selezionare la categoria dell'evento</label>
            <select name="cat" class="process-in" id="cat">
               <option value="Cinema" <?php if(isset($templateParams["eve"]) && ($templateParams["eve"][0]["categoria"] == "Cinema")) echo "selected"; ?>>Cinema</option>
               <option value="Concerti" <?php if(isset($templateParams["eve"]) && ($templateParams["eve"][0]["categoria"] == "Concerti")) echo "selected"; ?>>Concerti</option>
               <option value="Sport" <?php if(isset($templateParams["eve"]) && ($templateParams["eve"][0]["categoria"] == "Sport")) echo "selected"; ?>>Sport</option>
               <option value="Teatro" <?php if(isset($templateParams["eve"]) && ($templateParams["eve"][0]["categoria"] == "Teatro")) echo "selected"; ?>>Teatro</option>
            </select></div>
      </div>
      <div class="process-de">
            <div>Immagine </div>
            <div><label for="img" hidden>Inserire un'immagine per l'evento</label>
            <input type="file" name="img" class="process-in" id="img" />
            <input type="hidden" name="oldimg" value="<?php if(isset($templateParams["eve"])) echo $templateParams["eve"][0]["immagine"]; ?>" />
            <input type="hidden" name="action" value=<?php echo $templateParams["action"]; ?> />
            <input type="hidden" name="id" value=<?php if(isset($templateParams["eve"])) echo $templateParams["eve"][0]["id"]; ?> /></div>
      </div>
      <div class="process-btn">
      <label for="submit" hidden>Cliccare qui per inserire l'evento nel database</label>
         <input type="submit" name="submit" id="submit" value=<?php if(isset($templateParams["eve"])){echo "Aggiorna";} else {echo "Crea";} ?> />
      </div>
   </form>
</div>

<button class="log"><a href="login.php">Indietro</a></button>