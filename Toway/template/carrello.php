<div class="shopping-cart-container">
    <h1>Carrello</h1>

    <div class="shopping-cart">

        <div class="column-labels">
            <label class="product-image">Immagine</label>
            <label class="product-details">Prodotto</label>
            <label class="product-price">Prezzo</label>
            <label class="product-quantity">Quantit&aacute</label>
            <label class="product-removal">Rimuovi</label>
            <label class="product-line-price">Totale</label>
        </div>

        <?php foreach($templateParams["carrello"] as $tickets) : ?>
        <div class="product">
            <div class="product-image">
                <img src=<?php echo IMG_DIR.$tickets["immagine"]; ?> alt="immagine evento">
            </div>
            <div class="product-details">
                <div class="product-title" value=<?php echo $tickets["id"]; ?>><?php echo $tickets["nome"]; ?></div>
                <div class="product-category"><?php echo $tickets["citta"]; ?></div>
                <div class="product-street"><?php echo $tickets["via"]; ?></div>
                <div class="product-date"><?php echo changeDate($tickets["data_evento"]); ?></div>
                <div class="product-time"><?php echo $tickets["ora_inizio"]; ?></div>
                <p class="product-type">Posto: <?php echo $tickets["posto"]; ?></p>
                <div class="product-manager">Organizzatore: <?php echo $tickets["username"]; ?></div>
            </div>
            <div class="product-price"><?php echo $tickets["prezzo"]; ?></div>
            <div class="product-quantity">1</div>
            <div class="product-removal">
                <button class="remove-product">
                    Rimuovi
                </button>
            </div>
            <div class="product-line-price">0.00</div>
        </div>
    <?php endforeach; ?>

    </div>
</div>

<div class="back-to-cart">
    <span>Torna al carrello</span>
</div>

<div class="shipping" id="shipping">
    <h1>1. Spedizione</h1>
    <div class="shipping-choices">
        <label class="container mail">Invio tramite E-mail
        <label for="rmail" hidden>Cliccare qui per scegliere la spedzione via mail</label>
            <input type="radio" checked="checked" name="radio" id="rmail" value="mail" class="checked">
            <span class="checkmark"></span>
        </label>
        <label class="container home">Consegna a casa
        <label for="rhome" hidden>Cilccare qui per scegliere la spedizione a casa</label>
            <input type="radio"name="radio" id="rhome" value="home">
            <span class="checkmark"></span>
        </label>
    </div>
    <div class="shipping-type shipping-mail">
        <form action="" method="" name="email_form">
            <div class="nohome-name">
                <div>Email </div>
                <div><label for="email" hidden>Inserire la propria email</label>
                <input type="text" name="email" id="email" value=<?php echo $templateParams["email"]; ?> /></div>
            </div>
        </form>
    </div>
    <div class="shipping-type shipping-home">
        <form action="" method="" name="home_form">
            <div class="home-name">
                <div>Nome e Cognome </div>
                <div><label for="name" hidden>Inserire il prorpio nome e cognome</label>
                <input type="text" name="name" id="name"/></div>
            </div>
            <div class="home-address">
                <div>Indirizzo </div>
                <div><label for="address" hidden>Inserire il proprio indirizzo di casa</label>
                <input type="text" name="address" id="address"/></div>
            </div>
            <div class="home-province">
                <div>Provincia </div>
                <div><label for="province" hidden>Inserire la propria provincia</label>
                <input type="text" name="province" id="province"/></div>
            </div>
            <div class="home-city">
                <div>Citt&aacute </div>
                <div><label for="city" hidden>Inserire la propria citta di residenza</label>
                <input type="text" name="city" id="city"/></div>
            </div>
            <div class="home-cap">
                <div>CAP </div>
                <div><label for="cap" hidden>Inserire il proprio CAP</label>
                <input type="text" name="cap" id="cap"/></div>
            </div>
        </form>
    </div>
</div>

<div class="payment">
    <h1>2. Metodo di Pagamento</h1>
    <div class="payment-type">
        <form action="" method="" name="home_form">
            <div class="payment-card-number">
                <div>Numero carta </div>
                <div><label for="c-number" hidden>Inserire il numero della carta di credito</label>
                <input type="text" name="c-number" id="c-number"/></div>
            </div>
            <div class="payment-holder">
                <div>Intestatario carta </div>
                <div><label for="p-holder" hidden>Inserire il nome dell'intestatario</label>
                <input type="text" name="p-holder" id="p-holder"/></div>
            </div>
            <div class="payment-expiration-date">
                <div>Data di scadenza </div>
                <div><label for="expiration-date" hidden>Inserire la data di scadenza della carta</label>
                <input type="date" name="expiration-date" id="expiration-date"></div>
            </div>
            <div class="payment-cvc">
                <div>CVC </div>
                <div><label for="cvc" hidden>Inserire il CVC</label>
                <input type="text" name="cvc" id="cvc"/></div>
            </div>
        </form>
    </div>
</div>

<div class="incorrect-parameters">
    Attenzione! Alcuni parametri sono assenti.
</div>

<div class="totals">
    <div class="totals-item">
        <label>Spesa Parziale</label>
        <div class="totals-value" id="cart-subtotal">0.00</div>
    </div>
    <div class="totals-item">
        <label>Prevendita (5%)</label>
        <div class="totals-value" id="cart-tax">0.00</div>
    </div>
    <div class="totals-item totals-item-shipping">
        <label>Spedizione</label>
        <div class="totals-value" id="cart-shipping">0.00</div>
    </div>
    <div class="totals-item totals-item-total">
        <label>Totale</label>
        <div class="totals-value" id="cart-total">0.00</div>
    </div>
</div>
<div class="checkout-container">
    <button class="checkout">Procedi all'ordine</button>
</div>
    


<?php
// Call PHP function from javascript without ajax

function myphpfunction(){
    $mydata='Call by function declaration PHP';
    return $mydata;
}
?>