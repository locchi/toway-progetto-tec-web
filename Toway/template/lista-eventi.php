<?php foreach($templateParams["cose"] as $cose) : ?>
<article>
    <h2><?php if($cose == "recent") : ?> <?php echo "Eventi più recenti"; ?> <?php else : ?> <?php echo $cose; ?> <?php endif; ?></h2>
    <table class="lista-eventi table-<?php echo $cose; ?> <?php if(isset($showAll)){ echo $showAll;}?>">
    <?php foreach($templateParams[$cose] as $recente) : ?>     
        <?php if($recente["data_fine"]>date("Y-m-d")) : ?>
        <tr>   <!-- CON SHOW-ALL COME CLASSE AGGIUNTA, VERRANNO MOSTRATI TUTTI GLI EVENTI PRESENTI NELLA TABELLA -->
            <td class="image"><img src=<?php echo IMG_DIR.$recente["immagine"]; ?> alt="immagine evento" /></td>
            <td class="name"><?php echo $recente["nome"]; ?></td>
            <td class="td-date">Dal <?php echo $recente["data_inizio"]; ?></td>
            <td class="biglietti"><img src="./img/green.png" alt="" />Biglietti a <?php echo $recente["prezzo"]; ?> €</td>
            <td class="td-btn-ticket"><a href="pagina-evento.php?id=<?php echo $recente["id"]; ?>">Biglietti</a></td>
        </tr><?php endif; ?>
    <?php endforeach; ?>
    </table>
    <footer>
        <span>Mostra di più</span>
    </footer>
    </article>
<?php endforeach; ?>