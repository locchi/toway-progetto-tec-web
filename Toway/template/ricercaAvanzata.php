<aside class="ricerca-avanzata">
    <section>
        <h2>Ricerca Avanzata</h2>
        <ul>
            <li class="rsc scelta-citta">
                <img src="img/posizione.png" alt="" />
                <span>Inserisci la Citt&agrave</span>
            </li>
            <li class="citta-input">
                <label for="text-citta" hidden>Testo usato per filtrare tra le citt&agrave dove si tengono eventi</label>
                <input type="text" id="text-citta" name="text" placeholder="Citt&agrave" />
            </li>
            <li class="rsc scelta-data">
                <img src="img/calendario.png" alt="" />
                <span>Seleziona una Data</span>
            </li>
            <li class="data-input">
                <label for="date" hidden>Si scelga una data e si verifica se ci sono degli eventi in programma per quella data</label>
                <input type="date" name="date" id="date">
            </li>
            <li class="rsc scelta-categoria">
                <img src="img/categorie.png" alt="" />
                <span>Scegli una Categoria</span>
            </li>
            <li class="categoria-input">
                <label for="Concerti-c" hidden>Se spuntata, verranno visualizzati tutti i concerti</label>
                <input type="checkbox" name="Concerti" value="Concerti" id="Concerti-c"> Concerti<br/>
                <label for="Teatro-c" hidden>Se spuntata, verranno visualizzati tutti gli eventi in teatro</label>
                <input type="checkbox" name="Teatro" value="Teatro" id="Teatro-c"> Teatro<br/>
                <label for="Cinema-c" hidden>Se spuntata, verranno visualizzati tutti gli eventi tenuti in un cinema</label>
                <input type="checkbox" name="Cinema" value="Cinema" id="Cinema-c"> Cinema<br/>
                <label for="Sport-c" hidden>Se spuntata, verranno visualizzati tutti gli eventi sportivi</label>
                <input type="checkbox" name="Sport" value="Sport" id="Sport-c"> Sport
            </li>
            <li class="ricerca-input">
                <label for="text-ricerca-avanzata" hidden>Testo usato per filtrare tra i nomi degli eventi</label>
                <input type="text" id="text-ricerca-avanzata" name="ricerca" placeholder="Evento da cercare" />
            </li>
            <li class="rsc scelta-search">
                <img src="img/lente.png" alt="effettua la ricerca" />
                <a href="" id="btn-ricerca-a">Cerca</a>
            </li>
        </ul>
    </section>
</aside>