<div class="events">
    <div class="bestEvents">
        <h2>Eventi popolari</h2> 
        <ul>
            <?php foreach( $templateParams["top4"] as $top4) : ?><li value=<?php echo $top4["id_evento"]; ?>>
                <img src=<?php echo IMG_DIR.$top4["immagine"]; ?> alt="immagine evento" /><div class="bottom-text"><?php if(strlen($top4["nome"])>18) echo substr ($top4["nome"], 0, 16).".." ; else echo $top4["nome"]; ?></br>Dal <?php echo $top4["data_inizio"]; ?></div>
            </li><?php endforeach; ?>
        </ul>
    </div><div class="bestEventsWeek">
        <h2>Eventi popolari in questo periodo</h2> 
        <ul>
        <?php foreach( $templateParams["top4P"] as $top4P) : ?><li value=<?php echo $top4P["id_evento"]; ?>>
                <img src=<?php echo IMG_DIR.$top4P["immagine"]; ?> alt="immagine evento" /><div class="bottom-text"><?php if(strlen($top4P["nome"])>18) echo substr ($top4P["nome"], 0, 16).".." ; else echo $top4P["nome"]; ?></br>Dal <?php echo $top4P["data_inizio"]; ?></div>
            </li><?php endforeach; ?>
        </ul>
    </div>
</div>