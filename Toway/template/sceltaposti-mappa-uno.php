<div class="plane mapOne">
  <div class="cockpit">
    <h1>Selezionare un posto</h1>
  </div>
  <div class="exit exit--front fuselage">
    
  </div>
  <ol class="cabin fuselage">
    <li class="row row--1">
      <ol class="seats" type="A">
      <li class="seat">
          <input type="checkbox" <?php if($dbh->isOccupied($templateParams["id_evento"], $templateParams["date"], "1A")) echo "disabled"; ?> id="1A" />
          <label for="1A"><span>1A</span></label>
        </li>
        <li class="seat">
          <input type="checkbox" <?php if($dbh->isOccupied($templateParams["id_evento"], $templateParams["date"], "1B")) echo "disabled"; ?> id="1B" />
          <label for="1B"><span>1B</span></label>
        </li>
        <li class="seat">
          <input type="checkbox" <?php if($dbh->isOccupied($templateParams["id_evento"], $templateParams["date"], "1C")) echo "disabled"; ?> id="1C" />
          <label for="1C"><span>1C</span></label>
        </li>
        <li class="seat">
          <input type="checkbox" <?php if($dbh->isOccupied($templateParams["id_evento"], $templateParams["date"], "1D")) echo "disabled"; ?> id="1D" /> 
          <label for="1D"><span>1D</span></label>
        </li>
        <li class="seat">
          <input type="checkbox" <?php if($dbh->isOccupied($templateParams["id_evento"], $templateParams["date"], "1E")) echo "disabled"; ?> id="1E" />
          <label for="1E"><span>1E</span></label>
        </li>
        <li class="seat">
          <input type="checkbox" <?php if($dbh->isOccupied($templateParams["id_evento"], $templateParams["date"], "1F")) echo "disabled"; ?> id="1F" />
          <label for="1F"><span>1F</span></label>
        </li>
      </ol>
    </li>
    <li class="row row--2">
      <ol class="seats" type="A">
      <li class="seat">
          <input type="checkbox" <?php if($dbh->isOccupied($templateParams["id_evento"], $templateParams["date"], "2A")) echo "disabled"; ?> id="2A" />
          <label for="2A"><span>2A</span></label>
        </li>
        <li class="seat">
          <input type="checkbox" <?php if($dbh->isOccupied($templateParams["id_evento"], $templateParams["date"], "2B")) echo "disabled"; ?> id="2B" />
          <label for="2B"><span>2B</span></label>
        </li>
        <li class="seat">
          <input type="checkbox" <?php if($dbh->isOccupied($templateParams["id_evento"], $templateParams["date"], "2C")) echo "disabled"; ?> id="2C" />
          <label for="2C"><span>2C</span></label>
        </li>
        <li class="seat">
          <input type="checkbox" <?php if($dbh->isOccupied($templateParams["id_evento"], $templateParams["date"], "2D")) echo "disabled"; ?> id="2D" />
          <label for="2D"><span>2D</span></label>
        </li>
        <li class="seat">
          <input type="checkbox" <?php if($dbh->isOccupied($templateParams["id_evento"], $templateParams["date"], "2E")) echo "disabled"; ?> id="2E" />
          <label for="2E"><span>2E</span></label>
        </li>
        <li class="seat">
          <input type="checkbox" <?php if($dbh->isOccupied($templateParams["id_evento"], $templateParams["date"], "2F")) echo "disabled"; ?> id="2F" />
          <label for="2F"><span>2F</span></label>
        </li>
      </ol>
    </li>
    <li class="row row--3">
      <ol class="seats" type="A">
      <li class="seat">
          <input type="checkbox" <?php if($dbh->isOccupied($templateParams["id_evento"], $templateParams["date"], "3A")) echo "disabled"; ?> id="3A" />
          <label for="3A"><span>3A</span></label>
        </li>
        <li class="seat">
          <input type="checkbox" <?php if($dbh->isOccupied($templateParams["id_evento"], $templateParams["date"], "3B")) echo "disabled"; ?> id="3B" />
          <label for="3B"><span>3B</span></label>
        </li>
        <li class="seat">
          <input type="checkbox" <?php if($dbh->isOccupied($templateParams["id_evento"], $templateParams["date"], "3C")) echo "disabled"; ?> id="3C" />
          <label for="3C"><span>3C</span></label>
        </li>
        <li class="seat">
          <input type="checkbox" <?php if($dbh->isOccupied($templateParams["id_evento"], $templateParams["date"], "3D")) echo "disabled"; ?> id="3D" />
          <label for="3D"><span>3D</span></label>
        </li>
        <li class="seat">
          <input type="checkbox" <?php if($dbh->isOccupied($templateParams["id_evento"], $templateParams["date"], "3E")) echo "disabled"; ?> id="3E" />
          <label for="3E"><span>3E</span></label>
        </li>
        <li class="seat">
          <input type="checkbox" <?php if($dbh->isOccupied($templateParams["id_evento"], $templateParams["date"], "3F")) echo "disabled"; ?> id="3F" />
          <label for="3F"><span>3F</span></label>
        </li>
      </ol>
    </li>
    <li class="row row--4">
      <ol class="seats" type="A">
      <li class="seat">
          <input type="checkbox" <?php if($dbh->isOccupied($templateParams["id_evento"], $templateParams["date"], "4A")) echo "disabled"; ?> id="4A" />
          <label for="4A"><span>4A</span></label>
        </li>
        <li class="seat">
          <input type="checkbox" <?php if($dbh->isOccupied($templateParams["id_evento"], $templateParams["date"], "4B")) echo "disabled"; ?> id="4B" />
          <label for="4B"><span>4B</span></label>
        </li>
        <li class="seat">
          <input type="checkbox" <?php if($dbh->isOccupied($templateParams["id_evento"], $templateParams["date"], "4C")) echo "disabled"; ?> id="4C" />
          <label for="4C"><span>4C</span></label>
        </li>
        <li class="seat">
          <input type="checkbox" <?php if($dbh->isOccupied($templateParams["id_evento"], $templateParams["date"], "4D")) echo "disabled"; ?> id="4D" />
          <label for="4D"><span>4D</span></label>
        </li>
        <li class="seat">
          <input type="checkbox" <?php if($dbh->isOccupied($templateParams["id_evento"], $templateParams["date"], "4E")) echo "disabled"; ?> id="4E" />
          <label for="4E"><span>4E</span></label>
        </li>
        <li class="seat">
          <input type="checkbox" <?php if($dbh->isOccupied($templateParams["id_evento"], $templateParams["date"], "4F")) echo "disabled"; ?> id="4F" />
          <label for="4F"><span>4F</span></label>
        </li>
      </ol>
    </li>
    <li class="row row--5">
      <ol class="seats" type="A">
      <li class="seat">
          <input type="checkbox" <?php if($dbh->isOccupied($templateParams["id_evento"], $templateParams["date"], "5A")) echo "disabled"; ?> id="5A" />
          <label for="5A"><span>5A</span></label>
        </li>
        <li class="seat">
          <input type="checkbox" <?php if($dbh->isOccupied($templateParams["id_evento"], $templateParams["date"], "5B")) echo "disabled"; ?> id="5B" />
          <label for="5B"><span>5B</span></label>
        </li>
        <li class="seat">
          <input type="checkbox" <?php if($dbh->isOccupied($templateParams["id_evento"], $templateParams["date"], "5C")) echo "disabled"; ?> id="5C" />
          <label for="5C"><span>5C</span></label>
        </li>
        <li class="seat">
          <input type="checkbox" <?php if($dbh->isOccupied($templateParams["id_evento"], $templateParams["date"], "5D")) echo "disabled"; ?> id="5D" />
          <label for="5D"><span>5D</span></label>
        </li>
        <li class="seat">
          <input type="checkbox" <?php if($dbh->isOccupied($templateParams["id_evento"], $templateParams["date"], "5E")) echo "disabled"; ?> id="5E" />
          <label for="5E"><span>5E</span></label>
        </li>
        <li class="seat">
          <input type="checkbox" <?php if($dbh->isOccupied($templateParams["id_evento"], $templateParams["date"], "5F")) echo "disabled"; ?> id="5F" />
          <label for="5F"><span>5F</span></label>
        </li>
      </ol>
    </li>
    <li class="row row--6">
      <ol class="seats" type="A">
      <li class="seat">
          <input type="checkbox" <?php if($dbh->isOccupied($templateParams["id_evento"], $templateParams["date"], "6A")) echo "disabled"; ?> id="6A" />
          <label for="6A"><span>6A</span></label>
        </li>
        <li class="seat">
          <input type="checkbox" <?php if($dbh->isOccupied($templateParams["id_evento"], $templateParams["date"], "6B")) echo "disabled"; ?> id="6B" />
          <label for="6B"><span>6B</span></label>
        </li>
        <li class="seat">
          <input type="checkbox" <?php if($dbh->isOccupied($templateParams["id_evento"], $templateParams["date"], "6C")) echo "disabled"; ?> id="6C" />
          <label for="6C"><span>6C</span></label>
        </li>
        <li class="seat">
          <input type="checkbox" <?php if($dbh->isOccupied($templateParams["id_evento"], $templateParams["date"], "6D")) echo "disabled"; ?> id="6D" />
          <label for="6D"><span>6D</span></label>
        </li>
        <li class="seat">
          <input type="checkbox" <?php if($dbh->isOccupied($templateParams["id_evento"], $templateParams["date"], "6E")) echo "disabled"; ?> id="6E" />
          <label for="6E"><span>6E</span></label>
        </li>
        <li class="seat">
          <input type="checkbox" <?php if($dbh->isOccupied($templateParams["id_evento"], $templateParams["date"], "6F")) echo "disabled"; ?> id="6F" />
          <label for="6F"><span>6F</span></label>
        </li>
      </ol>
    </li>
    <li class="row row--7">
      <ol class="seats" type="A">
        <li class="seat">
          <input type="checkbox" <?php if($dbh->isOccupied($templateParams["id_evento"], $templateParams["date"], "7A")) echo "disabled"; ?> id="7A" />
          <label for="7A"><span>7A</span></label>
        </li>
        <li class="seat">
          <input type="checkbox" <?php if($dbh->isOccupied($templateParams["id_evento"], $templateParams["date"], "7B")) echo "disabled"; ?> id="7B" />
          <label for="7B"><span>7B</span></label>
        </li>
        <li class="seat">
          <input type="checkbox" <?php if($dbh->isOccupied($templateParams["id_evento"], $templateParams["date"], "7C")) echo "disabled"; ?> id="7C" />
          <label for="7C"><span>7C</span></label>
        </li>
        <li class="seat">
          <input type="checkbox" <?php if($dbh->isOccupied($templateParams["id_evento"], $templateParams["date"], "7D")) echo "disabled"; ?> id="7D" />
          <label for="7D"><span>7D</span></label>
        </li>
        <li class="seat">
          <input type="checkbox" <?php if($dbh->isOccupied($templateParams["id_evento"], $templateParams["date"], "7E")) echo "disabled"; ?> id="7E" />
          <label for="7E"><span>7E</span></label>
        </li>
        <li class="seat">
          <input type="checkbox" <?php if($dbh->isOccupied($templateParams["id_evento"], $templateParams["date"], "7F")) echo "disabled"; ?> id="7F" />
          <label for="7F"><span>7F</span></label>
        </li>
      </ol>
    </li>
    <li class="row row--8">
      <ol class="seats" type="A">
        <li class="seat">
          <input type="checkbox" <?php if($dbh->isOccupied($templateParams["id_evento"], $templateParams["date"], "8A")) echo "disabled"; ?> id="8A" />
          <label for="8A"><span>8A</span></label>
        </li>
        <li class="seat">
          <input type="checkbox" <?php if($dbh->isOccupied($templateParams["id_evento"], $templateParams["date"], "8B")) echo "disabled"; ?> id="8B" />
          <label for="8B"><span>8B</span></label>
        </li>
        <li class="seat">
          <input type="checkbox" <?php if($dbh->isOccupied($templateParams["id_evento"], $templateParams["date"], "8C")) echo "disabled"; ?> id="8C" />
          <label for="8C"><span>8C</span></label>
        </li>
        <li class="seat">
          <input type="checkbox" <?php if($dbh->isOccupied($templateParams["id_evento"], $templateParams["date"], "8D")) echo "disabled"; ?> id="8D" />
          <label for="8D"><span>8D</span></label>
        </li>
        <li class="seat">
          <input type="checkbox" <?php if($dbh->isOccupied($templateParams["id_evento"], $templateParams["date"], "8E")) echo "disabled"; ?> id="8E" />
          <label for="8E"><span>8E</span></label>
        </li>
        <li class="seat">
          <input type="checkbox" <?php if($dbh->isOccupied($templateParams["id_evento"], $templateParams["date"], "8F")) echo "disabled"; ?> id="8F" />
          <label for="8F"><span>8F</span></label>
        </li>
      </ol>
    </li>
    <li class="row row--9">
      <ol class="seats" type="A">
        <li class="seat">
          <input type="checkbox" <?php if($dbh->isOccupied($templateParams["id_evento"], $templateParams["date"], "9A")) echo "disabled"; ?> id="9A" />
          <label for="9A"><span>9A</span></label>
        </li>
        <li class="seat">
          <input type="checkbox" <?php if($dbh->isOccupied($templateParams["id_evento"], $templateParams["date"], "9B")) echo "disabled"; ?> id="9B" />
          <label for="9B"><span>9B</span></label>
        </li>
        <li class="seat">
          <input type="checkbox" <?php if($dbh->isOccupied($templateParams["id_evento"], $templateParams["date"], "9C")) echo "disabled"; ?> id="9C" />
          <label for="9C"><span>9C</span></label>
        </li>
        <li class="seat">
          <input type="checkbox" <?php if($dbh->isOccupied($templateParams["id_evento"], $templateParams["date"], "9D")) echo "disabled"; ?> id="9D" />
          <label for="9D"><span>9D</span></label>
        </li>
        <li class="seat">
          <input type="checkbox" <?php if($dbh->isOccupied($templateParams["id_evento"], $templateParams["date"], "9E")) echo "disabled"; ?> id="9E" />
          <label for="9E"><span>9E</span></label>
        </li>
        <li class="seat">
          <input type="checkbox" <?php if($dbh->isOccupied($templateParams["id_evento"], $templateParams["date"], "9F")) echo "disabled"; ?> id="9F" />
          <label for="9F"><span>9F</span></label>
        </li>
      </ol>
    </li>
    <li class="row row--10">
      <ol class="seats" type="A">
        <li class="seat">
          <input type="checkbox" <?php if($dbh->isOccupied($templateParams["id_evento"], $templateParams["date"], "10A")) echo "disabled"; ?> id="10A" />
          <label for="10A"><span>10A</span></label>
        </li>
        <li class="seat">
          <input type="checkbox" <?php if($dbh->isOccupied($templateParams["id_evento"], $templateParams["date"], "10B")) echo "disabled"; ?> id="10B" />
          <label for="10B"><span>10B</span></label>
        </li>
        <li class="seat">
          <input type="checkbox" <?php if($dbh->isOccupied($templateParams["id_evento"], $templateParams["date"], "10C")) echo "disabled"; ?> id="10C" />
          <label for="10C"><span>10C</span></label>
        </li>
        <li class="seat">
          <input type="checkbox" <?php if($dbh->isOccupied($templateParams["id_evento"], $templateParams["date"], "10D")) echo "disabled"; ?> id="10D" />
          <label for="10D"><span>10D</span></label>
        </li>
        <li class="seat">
          <input type="checkbox" <?php if($dbh->isOccupied($templateParams["id_evento"], $templateParams["date"], "10E")) echo "disabled"; ?> id="10E" />
          <label for="10E"><span>10E</span></label>
        </li>
        <li class="seat">
          <input type="checkbox" <?php if($dbh->isOccupied($templateParams["id_evento"], $templateParams["date"], "10F")) echo "disabled"; ?> id="10F" />
          <label for="10F"><span>10F</span></label>
        </li>
      </ol>
    </li>
  </ol>
  <div class="exit exit--back fuselage end">
    
  </div>
</div>
