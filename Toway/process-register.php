<?php
require_once 'secureBootstrap.php';
// Recupero la password criptata dal form di inserimento.
if($_POST["username"]!="" && isset($_POST["p"]) && $_POST["email"] != ""){ 
$password = $_POST['p'];
// Crea una chiave casuale
$random_salt = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), true));
// Crea una password usando la chiave appena creata.
$password = hash('sha512', $password.$random_salt);
if($dbh->checkEmail($_POST["email"])){
    if(!$dbh->register($_POST['username'], $_POST['email'], $password, $random_salt, $_POST['type'])){
        $templateParams["register"] = 1;
    }
}
else{
$templateParams["errorMail"] = 1;
}
}
else
$templateParams["errorparam"] = 1;
require "login.php";
?>