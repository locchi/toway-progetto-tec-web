<?php
require_once 'bootstrap.php';

//Base Template
$templateParams["titolo"] = "Toway - I tuoi eventi";
$templateParams["evento"] = "myEvents.php";
$templateParams["client"] = $dbh->getReservationByUId($_COOKIE["userId"]);
$arrId = array("0");
foreach($templateParams["client"] as $bigl){
    $x = $bigl["id"];
    if(!(array_search($x, $arrId)>0)){
        $templateParams[$x] = array();
        array_push($arrId, $x);
        foreach($templateParams["client"] as $b){
            if($b["id"]==$x){
                array_push($templateParams[$x], $b["posto"]);
            }
        }
    }
}

require 'template/base.php';
?>