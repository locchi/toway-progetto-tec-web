<?php
require_once 'bootstrap.php';
// Elimina tutti i valori della sessione.
$_SESSION = array();
// Recupera i parametri di sessione.
$params = session_get_cookie_params();
// Cancella i cookie attuali.
setcookie(session_name(), '', time() - 42000, $params["path"], $params["domain"], $params["secure"], $params["httponly"]);
// Cancella la sessione.
setcookie("userId", "", time() - 3600);
setcookie("username", "", time() - 3600);
setcookie("userType", "", time() - 3600);
header("Refresh:0; url=index.php");
?>