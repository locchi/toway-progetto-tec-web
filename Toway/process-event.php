<?php
require_once "bootstrap.php";

$nome = $_POST['nome'];
$desc = $_POST['desc'];
$prezzo = $_POST['prezzo'];
$citta = $_POST['citta'];
$via = $_POST['via'];
$startData = $_POST['start'];
$endData = $_POST['end'];
$cat = $_POST['cat'];
$aut = $_COOKIE["userId"];
$time = $_POST['time'];

if($nome!="" && $desc!="" && $prezzo>=0 && $via!="" && $startData!="" && $endData!="" &&  $startData<$endData){

if($_POST["action"]==1 && $_FILES["img"]!=""){
list($result, $msg) = uploadImage(IMG_DIR, $_FILES["img"]);
    if($result != 0){
        $img = $msg;
        if ($dbh->insertEvent($nome, $desc, $prezzo, $citta, $via, $startData, $endData, $cat, $aut, $img, $time)) {
            header('Location: login.php?insSuccess=1');
        }
        else 
            header('Location: creaEvento.php?error=1');
    }
}
else{
    $id = $_POST["id"];
    if(isset($_FILES["img"]) && strlen($_FILES["img"]["name"])>0){
        list($result, $msg) = uploadImage(IMG_DIR, $_FILES["img"]);
        if($result == 0){
            header("location: creaEvento.php?error=1");
        }
        $img = $msg;

    }
    else{
        $img = $_POST["oldimg"];
    }
    if($dbh->modifyEvent($id, $nome, $desc, $prezzo, $citta, $via, $startData, $endData, $cat, $img, $time)){
        header('Location: login.php?insSuccess=1');
        }
        else 
            header('Location: creaEvento.php?error=1');
    }
}
else
    header('Location: creaEvento.php?error=1');
?>