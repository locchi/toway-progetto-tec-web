<?php
require_once 'secureBootstrap.php';
$templateParams["titolo"] = "Toway - Login";
$templateParams["evento"] = "login-form.php";
if(isset($_POST['email'], $_POST['p'])) { 
   $email = $_POST['email'];
   $password = $_POST['p']; // Recupero la password criptata.
   $id = $dbh->login($email, $password);
   $username = $dbh->getUserName($id);

   if($username != ""){
      $templateParams["userId"] = $id;
      $templateParams["username"] = $username;
      $userType = $dbh->getUserType($id);
      $templateParams["userType"] = $userType;
      setcookie("userId", $id);
      header('Location: login.php');
   } else {
      // Login fallito
      $templateParams["error"] = 1;
   }
} else { 
   // Le variabili corrette non sono state inviate a questa pagina dal metodo POST.
   echo 'Invalid Request';
}
require "template/base.php";
?>