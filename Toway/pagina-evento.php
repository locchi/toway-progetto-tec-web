<?php
require_once 'bootstrap.php';

//Base Template
$templateParams["titolo"] = "Toway - Evento"; //  AGGIUNGERE IL NOME DELL'EVENTO
$templateParams["evento"] = "evento.php";
$event = $dbh->getEventById($_GET["id"]);
if(isset($_GET["mod"]))
    $dbh->modifyReservation($_COOKIE["userId"], $_GET["id"], 0);
if(isset($_GET["tic"]))
    $templateParams["tic"] = 1;
$templateParams["id"] = $event[0]["id"];
$templateParams["imm"] = $event[0]["immagine"];
$templateParams["titolo"] = $event[0]["nome"];
$templateParams["desc"] = $event[0]["descrizione"];
$templateParams["localita"] = $event[0]["citta"];
$templateParams["prezzo"] = $event[0]["prezzo"];
$templateParams["dataIn"] = $event[0]["data_inizio"];
$templateParams["dataFin"] = $event[0]["data_fine"];
$templateParams["ora_inizio"] = $event[0]["ora_inizio"];
$dataI = explode("-", $templateParams["dataIn"]);
$dataF = explode("-", $templateParams["dataFin"]);
$numGiorni = getDaysInMonth((int)$dataI[0], (int)$dataI[1]);
$giornoI = (int)$dataI[2];
$giornoF = (int)$dataF[2];
if($giornoF<$giornoI){
    $totGiorn = $giornoF + $numGiorni - $giornoI;
}
else 
    $totGiorn = $giornoF - $giornoI;
$templateParams["tot"] = $totGiorn;
$templateParams["dayMonth"] = $numGiorni;

require 'template/base.php';
?>