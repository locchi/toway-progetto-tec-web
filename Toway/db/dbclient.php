<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
class DatabaseHelper{
    private $db;

    public function __construct($servername, $username, $password, $dbname){
        $this->db = new mysqli($servername, $username, $password, $dbname);
        if ($this->db->connect_error) {
            die("Connection failed: " . $db->connect_error);
        }        
    }

    public function login($email, $password) {
        // Usando statement sql 'prepared' non sarà possibile attuare un attacco di tipo SQL injection.
        if ($stmt = $this->db->prepare("SELECT id, username, password, salt, type FROM utenti WHERE email = ? LIMIT 1")) { 
           $stmt->bind_param('s', $email); // esegue il bind del parametro '$email'.
           $stmt->execute(); // esegue la query appena creata.
           $stmt->store_result();
           $stmt->bind_result($user_id, $username, $db_password, $salt, $type); // recupera il risultato della query e lo memorizza nelle relative variabili.
           $stmt->fetch();
           $password = hash('sha512', $password.$salt); // codifica la password usando una chiave univoca.
           if($stmt->num_rows == 1) { // se l'utente esiste
              if($db_password == $password) { // Verifica che la password memorizzata nel database corrisponda alla password fornita dall'utente.
                 // Password corretta!            
                   // Login eseguito con successo.
                    return $user_id;    
              } else {
                 // Password incorretta.
                 return 0;
              }
           }
           } else {
              // L'utente inserito non esiste.
              return 0;
           }
        }

    
    public function register($username, $email, $password, $salt, $type){
        $insert_stmt = $this->db->prepare("INSERT INTO utenti (username, email, password, salt, type) VALUES (?, ?, ?, ?, ?)"); 
        $insert_stmt->bind_param('sssss', $username, $email, $password, $salt, $type); 
        // Esegui la query ottenuta.
        $insert_stmt->execute();
    }

    public function getUserType ($id){
        $stmt = $this->db->prepare("SELECT type from utenti WHERE Id = ? LIMIT 1"); 
        $stmt->bind_param('i', $id); 
        // Esegui la query ottenuta.
        $stmt->execute();
        $stmt->store_result();
        $stmt->bind_result($type); 
        $stmt->fetch();

        return $type;
    }

    public function getUserName ($id){
        $stmt = $this->db->prepare("SELECT username from utenti WHERE Id = ? LIMIT 1"); 
        $stmt->bind_param('i', $id); 
        // Esegui la query ottenuta.
        $stmt->execute();
        $stmt->store_result();
        $stmt->bind_result($name); 
        $stmt->fetch();

        return $name;
    }

    public function checkEmail($email){
        $stmt = $this->db->prepare("SELECT email from utenti WHERE email = ? LIMIT 1"); 
        $stmt->bind_param('s', $email); 
        // Esegui la query ottenuta.
        $stmt->execute();
        $stmt->store_result();
        if($stmt->num_rows == 1)
            return false;
        return true;
    }

    public function getEmail($id){
        $stmt = $this->db->prepare("SELECT email from utenti WHERE Id = ? LIMIT 1"); 
        $stmt->bind_param('i', $id); 
        // Esegui la query ottenuta.
        $stmt->execute();
        $stmt->store_result();
        $stmt->bind_result($email); 
        $stmt->fetch();

        return $email;
    }

    public function insertEvent($nome, $desc, $prezzo, $citta, $via, $startData, $endData, $cat, $aut, $img, $time){
        if($insert_stmt = $this->db->prepare("INSERT INTO eventi (nome, immagine, descrizione, prezzo, citta, via, organizzatore, data_inizio, data_fine, categoria, ora_inizio) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")){
            $insert_stmt->bind_param('sssisssssss', $nome, $img, $desc, $prezzo, $citta, $via, $aut, $startData, $endData, $cat, $time); 
            // Esegui la query ottenuta.
            return $insert_stmt->execute();
        }
        else {
            $error = $this->db->errno . ' ' . $this->db->error;
            echo $error;
            return false;
        }
    }

    public function modifyEvent($id, $nome, $desc, $prezzo, $citta, $via, $startData, $endData, $cat, $img, $time){
        if($insert_stmt = $this->db->prepare("UPDATE eventi SET nome=?, immagine=?, descrizione=?, prezzo=?, citta=?, via=?, data_inizio=?, data_fine=?, categoria=?, ora_inizio=? WHERE id = ?")){
            $insert_stmt->bind_param('sssissssssi', $nome, $img, $desc, $prezzo, $citta, $via, $startData, $endData, $cat, $time, $id); 
            // Esegui la query ottenuta.
            if($insert_stmt->execute()){
                return $this->modifyAllReservations($id, 1);
            }
            else {
                return false;
            }
        }
        else {
            $error = $this->db->errno . ' ' . $this->db->error;
            echo $error;
            return false;
        }
    }

    public function getEvents($n){
        $stmt = $this->db->prepare("SELECT * from eventi LIMIT ?"); 
        $stmt->bind_param('i', $n); 
        
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getEventById($id){
        $stmt = $this->db->prepare("SELECT * from eventi WHERE id = ? LIMIT 1"); 
        $stmt->bind_param('i', $id); 
        
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getEventByName($name){
        $stmt = $this->db->prepare("SELECT * from eventi WHERE nome = ? LIMIT 1"); 
        $stmt->bind_param('s', $name); 
        
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getEventsByGestor($id){
        $stmt = $this->db->prepare("SELECT * from eventi WHERE organizzatore = ?"); 
        $stmt->bind_param('i', $id); 
        
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getEventByAuthor($name){
        $stmt = $this->db->prepare("SELECT * from eventi, utenti WHERE organizzatore = utenti.id AND username = ?"); 
        $stmt->bind_param('s', $name); 
        
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }


    public function getDatesOfEvent($id){
        $stmt = $this->db->prepare("SELECT data_inizio, data_fine from eventi WHERE id = ?"); 
        $stmt->bind_param('i', $id); 
        
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getEventsByLocale($locale){
        $stmt = $this->db->prepare("SELECT * from eventi WHERE citta = ?"); 
        $stmt->bind_param('s', $locale); 
        
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getEventsByCategory($cat){
        $stmt = $this->db->prepare("SELECT * from eventi WHERE categoria = ? AND data_fine > ? ORDER BY nome"); 
        $dat = date("Y-m-d");
        $stmt->bind_param('ss', $cat, $dat); 
        
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getEventsByDate($date){
        $stmt = $this->db->prepare("SELECT * FROM eventi WHERE data_inizio > ? OR data_fine > ?"); 
        $stmt->bind_param('ss', $date, $date); 
        
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getEventsRecent(){
        $stmt = $this->db->prepare("SELECT * FROM eventi ORDER BY id DESC LIMIT 5"); 
        
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function insertReservation($uid, $eid, $date, $posto){
        if($insert_stmt = $this->db->prepare("INSERT INTO prenotazioni (id_utente, id_evento, biglietto_acquistato, data_evento, posto, changed) VALUES (?, ?, 0, ?, ?, 0)")){
            $insert_stmt->bind_param('iiss', $uid, $eid, $date, $posto); 
            // Esegui la query ottenuta.
            return $insert_stmt->execute();
        }
        else {
            $error = $this->db->errno . ' ' . $this->db->error;
            echo $error;
            return false;
        }
    }

    public function modifyAllReservations($eid, $c){
        if($insert_stmt = $this->db->prepare("UPDATE prenotazioni SET changed=? WHERE id_evento = ?")){
            $insert_stmt->bind_param('ii', $c, $eid); 
            // Esegui la query ottenuta.
            return $insert_stmt->execute();
        }
        else {
            $error = $this->db->errno . ' ' . $this->db->error;
            echo $error;
            return false;
        }
    }

    public function getChangedById($uid){
        $stmt = $this->db->prepare("SELECT changed from prenotazioni WHERE id_utente = ? ORDER BY changed DESC LIMIT 1"); 
        $stmt->bind_param('i', $uid); 
        
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function modifyReservation($uid, $eid, $c){
        if($insert_stmt = $this->db->prepare("UPDATE prenotazioni SET changed=? WHERE id_evento = ? AND id_utente=?")){
            $insert_stmt->bind_param('iii', $c, $eid, $uid); 
            // Esegui la query ottenuta.
            return $insert_stmt->execute();
        }
        else {
            $error = $this->db->errno . ' ' . $this->db->error;
            echo $error;
            return false;
        }
    }

    public function getReservationByUId($id){
        $stmt = $this->db->prepare("SELECT eventi.id, nome, immagine, id_evento, data_evento, posto, changed from eventi, prenotazioni WHERE id_utente = ? AND id_evento = eventi.id AND biglietto_acquistato = 1"); 
        $stmt->bind_param('i', $id); 
        
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getUnacquistedTickets($uid){
        $stmt = $this->db->prepare("SELECT prenotazioni.id, nome, immagine, citta, username, via, prezzo, categoria, id_evento, data_evento, posto, ora_inizio, changed from utenti, eventi, prenotazioni WHERE id_utente = ? AND id_evento = eventi.id AND eventi.organizzatore = utenti.id AND biglietto_acquistato = 0"); 
        $stmt->bind_param('i', $uid); 
        
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function buyTickets($id){
        if($insert_stmt = $this->db->prepare("UPDATE prenotazioni SET biglietto_acquistato=1 WHERE id_utente = ?")){
            $insert_stmt->bind_param('i', $id); 
            // Esegui la query ottenuta.
            return $insert_stmt->execute();
        }
        else {
            $error = $this->db->errno . ' ' . $this->db->error;
            echo $error;
            return false;
        }
    }

    public function deleteReservation($pid){
        if($insert_stmt = $this->db->prepare("DELETE FROM prenotazioni WHERE id = ?")){
            $insert_stmt->bind_param('i', $pid); 
            // Esegui la query ottenuta.
            return $insert_stmt->execute();
        }
        else {
            $error = $this->db->errno . ' ' . $this->db->error;
            echo $error;
            return false;
        }
    }

    public function getTop4(){
        $stmt = $this->db->prepare("SELECT id_evento, nome, immagine, data_inizio, COUNT(*) as preno FROM prenotazioni, eventi WHERE id_evento = eventi.id AND data_evento > ? GROUP BY id_evento ORDER BY preno DESC, data_inizio DESC LIMIT 4"); 
        $data = date("Y-m-d");
        $stmt->bind_param('s', $data); 
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
        
    }

    public function getTop4InPeriod(){
        $stmt = $this->db->prepare("SELECT id_evento, nome, immagine, data_inizio, COUNT(*) as preno FROM prenotazioni, eventi WHERE id_evento = eventi.id AND data_evento BETWEEN ? AND ? GROUP BY id_evento ORDER BY preno DESC LIMIT 4"); 
        
        $data = date("Y-m-d");
        $data10 = date10($data);
        $stmt->bind_param('ss', $data, $data10); 
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
        
    }

    public function getOccupiedSeats($eid, $date){
        $stmt = $this->db->prepare("SELECT posto FROM prenotazioni WHERE id_evento = ? AND data_evento = ?"); 
        
        $stmt->bind_param('is', $eid, $date); 
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function isOccupied($eid, $date, $posto){
        $stmt = $this->db->prepare("SELECT * FROM prenotazioni WHERE id_evento = ? AND data_evento = ? AND posto = ?"); 
        
        $stmt->bind_param('iss', $eid, $date, $posto); 
        $stmt->execute();
        $result = $stmt->get_result();
        $rowcount=mysqli_num_rows($result);
        if($rowcount == 1)
            return true;
        return false;
    }

    public function getPosti($citta){
        $stmt = $this->db->prepare("SELECT posti FROM piantine, posti WHERE citta = ? AND posti.piantina = piantine.id");
        $stmt->bind_param('s', $citta);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }
     

    public function insertNewCity($city, $piantina){
        if($insert_stmt = $this->db->prepare("INSERT INTO posti VALUES (?, ?)")){
            $insert_stmt->bind_param('si', $city, $piantina); 
            // Esegui la query ottenuta.
            return $insert_stmt->execute();
        }
        else {
            $error = $this->db->errno . ' ' . $this->db->error;
            echo $error;
            return false;
        }
    }

    public function getCities(){
        $stmt = $this->db->prepare("SELECT citta, posti FROM posti, piantine WHERE posti.piantina = piantine.id"); 
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getPiantina($id){
        $stmt = $this->db->prepare("SELECT piantina FROM eventi, posti WHERE eventi.id = ? AND eventi.citta = posti.citta"); 
        $stmt->bind_param('i', $id); 
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function advancedSearch($city, $date, $search, $Cinema, $Teatro, $Concerti, $Sport){
        $query = "SELECT * FROM eventi ";
        $first = 0;
        $cat = 0;
        $d = 0;
        $cos = array();
        $bind = "";
        if($date != 0){
            $d = 1;
            $query = $query."WHERE (data_inizio <= ? AND data_fine >= ?) ";
            $first = $first + 1;
            $bind = $bind."ss";
            array_push($cos, $date);
        }
        if($city != ""){
            if($first == 0)
                $query = $query."WHERE citta LIKE ? ";
            else
                $query = $query."AND citta LIKE ? ";
            $first = $first + 1;
            $bind = $bind."s";
            array_push($cos, $city);
        }
        if($search != ""){
            if($first == 0){
                $query = $query."WHERE nome LIKE ? ";
            }
            else {
                $query = $query."AND nome LIKE ? ";
            }
            $first = $first + 1;
            $bind = $bind."s";
            array_push($cos, $search);
        }
        if($Cinema != 0){
            if($first == 0){
                $query = $query."WHERE (categoria = ? ";
            }
            else {
                $query = $query."AND (categoria = ? ";
            }
            $first = $first + 1;
            $bind = $bind."s";
            $cat = $cat + 1;
            array_push($cos, "Cinema");
        }
        if($Sport != 0){
            if($first == 0){
                $query = $query."WHERE (categoria = ? ";
            }
            else if($cat == 0){
                    $query = $query."AND (categoria = ? ";
                 }
                 else {
                    $query = $query."OR categoria = ? ";
                 }
            $first = $first + 1;
            $bind = $bind."s";
            $cat = $cat + 1;
            array_push($cos, "Sport");
        }
        if($Concerti != 0){
            if($first == 0){
                $query = $query."WHERE (categoria = ? ";
            }
            else if($cat == 0){
                    $query = $query."AND (categoria = ? ";
                 }
                 else {
                    $query = $query."OR categoria = ? ";
                 }
            $first = $first + 1;
            $bind = $bind."s";
            $cat = $cat + 1;
            array_push($cos, "Concerti");
        }
        if($Teatro != 0){
            if($first == 0){
                $query = $query."WHERE (categoria = ? ";
            }
            else if($cat == 0){
                    $query = $query."AND (categoria = ? ";
                 }
                 else {
                    $query = $query."OR categoria = ? ";
                 }
            $first = $first + 1;
            $bind = $bind."s";
            $cat = $cat + 1;
            array_push($cos, "Teatro");
        }
        if($cat > 0){
            $query = $query.")";
        }
        switch(count($cos)){
            case 0: {
                $stmt = $this->db->prepare("SELECT * FROM eventi"); 
                $stmt->execute();
                $result = $stmt->get_result();
                return $result->fetch_all(MYSQLI_ASSOC);
            }

            case 1: {
                return $this->bind1($query, $bind, $cos[0], $d);
            }
            case 2: {
                return $this->bind2($query, $bind, $cos[0], $cos[1], $d);
            }
            case 3: {
                return $this->bind3($query, $bind, $cos[0], $cos[1], $cos[2], $d);
            }
            case 4: {
                return $this->bind4($query, $bind, $cos[0], $cos[1], $cos[2], $cos[3], $d);
            }
            case 5: {
                return $this->bind5($query, $bind, $cos[0], $cos[1], $cos[2], $cos[3], $cos[4], $d);
            }
            case 6: {
                return $this->bind6($query, $bind, $cos[0], $cos[1], $cos[2], $cos[3], $cos[4], $cos[5], $d);
            }
            default: {
                return $this->bind7($query, $bind, $cos[0], $cos[1], $cos[2], $cos[3], $cos[4], $cos[5], $cos[6], $d);
            }
        }
    }

    public function bind1($query, $bind, $p1, $d){
        $stmt = $this->db->prepare($query);
        if($d == 1)
            $stmt->bind_param($bind, $p1, $p1);
        else
            $stmt->bind_param($bind, $p1 );
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function bind2($query, $bind, $p1, $p2, $d){
        $stmt = $this->db->prepare($query);
        if($d == 1)
            $stmt->bind_param($bind, $p1, $p1, $p2);
        else
            $stmt->bind_param($bind, $p1, $p2 );
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function bind3($query, $bind, $p1, $p2, $p3, $d){
        $stmt = $this->db->prepare($query);
        if($d == 1)
            $stmt->bind_param($bind, $p1, $p1, $p2, $p3);
        else
            $stmt->bind_param($bind, $p1, $p2, $p3 );
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function bind4($query, $bind, $p1, $p2, $p3, $p4, $d){
        $stmt = $this->db->prepare($query);
        if($d == 1)
            $stmt->bind_param($bind, $p1, $p1, $p2, $p3, $p4);
        else
            $stmt->bind_param($bind, $p1, $p2, $p3, $p4);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function bind5($query, $bind, $p1, $p2, $p3, $p4, $p5, $d){
        $stmt = $this->db->prepare($query);
        if($d == 1)
            $stmt->bind_param($bind, $p1, $p1, $p2, $p3, $p4, $p5);
        else
            $stmt->bind_param($bind, $p1, $p2, $p3, $p4, $p5 );
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function bind6($query, $bind, $p1, $p2, $p3, $p4, $p5, $p6, $d){
        $stmt = $this->db->prepare($query);
        if($d == 1)
            $stmt->bind_param($bind, $p1, $p1, $p2, $p3, $p4, $p5, $p6);
        else
            $stmt->bind_param($bind, $p1, $p2, $p3, $p4, $p5, $p6);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function bind7($query, $bind, $p1, $p2, $p3, $p4, $p5, $p6, $p7, $d){
        $stmt = $this->db->prepare($query);
        if($d == 1)
            $stmt->bind_param($bind, $p1, $p1, $p2, $p3, $p4, $p5, $p6, $p7);
        else
            $stmt->bind_param($bind, $p1, $p2, $p3, $p4, $p5, $p6, $p7 );
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }
}
?>