<?php
require_once 'bootstrap.php';

//Base Template
$templateParams["titolo"] = "Toway - Crea il tuo evento";
$templateParams["evento"] = "myEvents.php";
$templateParams["gestor"] = $dbh->getEventsByGestor($_COOKIE["userId"]);

require 'template/base.php';
?>