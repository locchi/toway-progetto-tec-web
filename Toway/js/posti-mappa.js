function getFreeSeats()
{
    let freeSeats = [];
    $('li.seat').each(function () {
        const input = $(this).children('input');
        if(!input.is(':disabled')){
            freeSeats.push(input.attr("id"));
        }
    });
    
    return freeSeats;
}

function getBestSeats(freeSeats, nBestSeats){
    let i = 0;
    let string = ""
    for(i=0; i< nBestSeats; i++){
        if(i > 0){
            string +=",";
        }
        string += freeSeats[i];
    }

    return string;
}

function getMapSeats(postiSel){
    let string = "";
    for(i=0; i< postiSel.length; i++){
        if(i > 0){
            string +=",";
        }
        string += postiSel[i];
    }

    return string;
}

$(document).ready(function(){
    let postiSel = [];
    let mod = 0;

    const freeSeats = getFreeSeats();
    let nBestSeats = 0;

    $(function() {
        $("input:checkbox").change(function(){
            postiSel = [];
            $("input:checkbox").each(function(){
                if($(this).is(":checked")){
                    postiSel.push($(this).attr("id"));
                }
            });
            $("div.mappa > div.biglietti-selezionati > p").remove();
            if(postiSel != 0){
                $("div.mappa > div.biglietti-selezionati").prepend("<p>Biglietti Selezionati:<br/>"+postiSel+"</p>");
            }
            else{
                $("div.mappa > div.biglietti-selezionati").prepend("<p>Biglietti Selezionati:<br/>Nessuno</p>");
            }
            const bigliettiSel = postiSel.length;
            let costoSingolo = parseFloat($('.prezzo-biglietto').text());
            const costoTotale = bigliettiSel * costoSingolo;
            let stringaCosto = "";
            if(bigliettiSel == 1){
                stringaCosto = "<p>"+bigliettiSel+" biglietto. Totale: "+costoTotale+" &#8364</p>";
            }
            else if(bigliettiSel > 1){
                stringaCosto = "<p>"+bigliettiSel+" biglietti. Totale: "+costoTotale+" &#8364</p>"
            }
            if($('div.biglietti-selezionati > div').css('display') == 'none') {
                $("div.biglietti-selezionati > div").show();
            }
            $("div.mappa > div.biglietti-selezionati > div > p").remove();
            $("div.mappa > div.biglietti-selezionati > div").append(stringaCosto);

            if(postiSel.length != 0){
                $("form.aggiunta-carrello").slideDown();
            }
            else{
                $("form.aggiunta-carrello").slideUp();
            }
        });    
    });

    $("div.scelta-migliorposto > input:button").click(function(){
        $("div.mappa").slideUp();
        $("div.migliorposto").slideDown();
        mod = 0;
        if(nBestSeats != 0){
            $("form.aggiunta-carrello").slideDown();
        }
        else{
            $("form.aggiunta-carrello").slideUp();
        }
    });

    $("div.scelta-mappa > input:button").click(function(){
        $("div.migliorposto").slideUp();
        $("div.mappa").slideDown();
        mod = 1;
        if(postiSel.length != 0){
            $("form.aggiunta-carrello").slideDown();
        }
        else{
            $("form.aggiunta-carrello").slideUp();
        }
    });

    $("#hall").change(function(){
        nBestSeats = $(this).val();
        if(nBestSeats != 0){
            $("form.aggiunta-carrello").slideDown();
        }
        else{
            $("form.aggiunta-carrello").slideUp();
        }
    });

    $('form.aggiunta-carrello > a').click( function() {
        if(mod == 0){
            window.location = "pagina-carrello.php?id="+$('div.eid').attr("value")+"&date="+$('div.date').attr("value")+"&nposti="+getBestSeats(freeSeats, nBestSeats);
        }
        else {
            window.location = "pagina-carrello.php?id="+$('div.eid').attr("value")+"&date="+$('div.date').attr("value")+"&nposti="+getMapSeats(postiSel);
        }
    });
});