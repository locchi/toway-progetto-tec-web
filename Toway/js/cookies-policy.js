function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
  }

function accepted(){
    $("#seen").addClass("seen");
    $('#cookies-policy').attr('hidden', "true");
    $('#cookies-policy').fadeOut("slow");
    document.cookie = "cookies=1";
}

function rejected(){
    $("seen").addClass("seen");
    $('#cookies-policy').attr('hidden', "true");
    $('#cookies-policy').fadeOut("slow");
    document.cookie = "cookies=1";
}

$(document).ready(function(){
    const seen = getCookie("cookies");
   if(!($('#seen').hasClass("seen") || seen!="")){
        $('#cookies-policy').removeAttr('hidden');
        $('#cookies-policy').fadeIn("slow");
   }
   $("#accept").click(function(){
        accepted();
   });
   $("#reject").click(function(){
        rejected();
});
});