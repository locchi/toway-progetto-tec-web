let itemQuantity = 0;

function aggiornaCarrello(){
    itemQuantity = $('span.badge').attr("value");   //Biglietti presenti nel carrello

    if(itemQuantity > 0){
        $('span.badge').css("display", "inherit");
        $('span.badge').html(itemQuantity);
        if(itemQuantity > 9){
            $('span.badge').html('9');
        }
    }
}

$(document).ready(function(){

    let menuDown = false;
    let ricercaAvanzataDown = false;
    let sceltaCittaDown = false;
    let sceltaDataDown = false;
    let sceltaCategoriaDown = false;

    let changedRACss = false;

    if($('div.login').hasClass("logged")) { 
        $('.login-logged').show();
        $('.login-not-logged').hide();
    } 

    aggiornaCarrello();

    $('div.logged').click( function() {
        window.location = 'login.php';
    });

    $('nav.menu-navigatore li:first-child').click( function() {
        window.location = 'login.php';
    });

    $('img.logo-sito').click( function() {
        window.location = 'index.php';
    });

    $('div.menu-icona').click( function() {
        if(menuDown){
            $('div.menu').slideUp();
            if(ricercaAvanzataDown){
                $('aside.ricerca-avanzata').slideUp();
                $('li.barra-ricerca').slideDown();
                ricercaAvanzataDown = false;
            }
        }
        else {
            $('div.menu').slideDown();
        }
        menuDown = !menuDown;
    });

    $('nav.menu-navigatore li.ricerca').click( function() {
        if(ricercaAvanzataDown){
            $('aside.ricerca-avanzata').slideUp();
            $('li.barra-ricerca').slideDown();
        }
        else {
            $('li.barra-ricerca').slideUp();
            $('aside.ricerca-avanzata').slideDown();
        }
        ricercaAvanzataDown = !ricercaAvanzataDown;
    });

    $('li.scelta-citta').click( function() {
        if(sceltaCittaDown){
            $('li.citta-input').slideUp();
        }
        else {
            $('li.citta-input').slideDown();
        }
        sceltaCittaDown = !sceltaCittaDown;
    });

    $('li.scelta-data').click( function() {
        if(sceltaDataDown){
            $('li.data-input').slideUp();
        }
        else {
            $('li.data-input').slideDown();
        }
        sceltaDataDown = !sceltaDataDown;
    });

    $('li.scelta-categoria').click( function() {
        if(sceltaCategoriaDown){
            $('li.categoria-input').slideUp();
        }
        else {
            $('li.categoria-input').slideDown();
        }
        sceltaCategoriaDown = !sceltaCategoriaDown;
    });

    $('div.events ul li').click( function() {
        window.location = "pagina-evento.php?id="+$(this).attr("value");
    });

    $('button.log').click( function() {
        window.location = $(this).children("a").attr("href");
    });

    $(window).resize(function() {
        // This will execute whenever the window is resized
        if ($(window).width() >= 1100) {
            if(menuDown){
                if(ricercaAvanzataDown){
                    $('li.barra-ricerca').show();
                    ricercaAvanzataDown = false;
                }
                $('div.menu').hide();
                menuDown = false;
            }
            $('aside.ricerca-avanzata').css("display", "table-cell");
        }
        else if ($(window).width() >= 751) {
            $('aside.ricerca-avanzata').css("display", "block");
            $('aside.ricerca-avanzata').hide();
            if(menuDown){
                if(ricercaAvanzataDown){
                    $('li.barra-ricerca').show();
                    ricercaAvanzataDown = false;
                }
                $('div.menu').hide();
                menuDown = false;
            }
        }
        else if (!menuDown){
            $('aside.ricerca-avanzata').css("display", "block");
            $('aside.ricerca-avanzata').hide();
        }
    });
});