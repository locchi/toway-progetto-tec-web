function getContent(){
    let query = "eventiCercati.php";

    const search = $('li.barra-ricerca input').attr("value");

    if(search != null && search != ""){
        query += "?search=%" + search + "%";
    }

    return query;
}

function getAdvanceContent(){
    let query = "eventiCercati.php";
    let changedQ = false;

    const city = $('li.citta-input input').attr("value");
    const date = $('li.data-input input').attr("value");

    if(city != null && city != ""){
        query += "?city=" + city + "%";
        changedQ = true;
    }
    if(date != null && date !=""){
        query += changedQuery(changedQ) + "data=" + date;
        changedQ = true;
    }

    const search = $('li.ricerca-input input').attr("value");

    if(search != null && search != ""){
        query += changedQuery(changedQ) + "search=%" + search + "%";
        changedQ = true;
    }

    const Concerti = document.getElementById("Concerti-c").checked == true ? "Concerti=1" : "";
    const Teatro = document.getElementById("Teatro-c").checked == true ? "Teatro=1" : "";
    const Cinema = document.getElementById("Cinema-c").checked == true ? "Cinema=1" : "";
    const Sport = document.getElementById("Sport-c").checked == true ? "Sport=1" : "";

    if(Concerti != ""){
        query += changedQuery(changedQ) + Concerti;
        changedQ = true;
    }
    if(Teatro != ""){
        query += changedQuery(changedQ) + Teatro;
        changedQ = true;
    }
    if(Cinema != ""){
        query += changedQuery(changedQ) + Cinema;
        changedQ = true;
    }
    if(Sport != ""){
        query += changedQuery(changedQ) + Sport;
        changedQ = true;
    }
    return query;
}

function changedQuery(changedQ){
    if(changedQ){
        return "&";
    }
    else{
        return "?";
    }
}

$(document).ready(function(){

    $('li.scelta-search').click( function() {
        const query = getAdvanceContent();
        document.getElementById("btn-ricerca-a").href = query; 
        window.location = query;
    });

    $('a#btn-ricerca').click( function() {
        const query = getContent();
        document.getElementById("btn-ricerca").href = query; 
    });

});