/* Set rates + misc */
let taxRate = 0.05;
let shippingRate = 2.00; 
let shippingHome = false;
let fadeTime = 500;

let productNumbers = 0;
let shipping = 0;
let total = 0;

let mode = 0;

window.onload = function() {
    update();
    if(total == 0){
        $('.checkout').hide();
    }
};

/* Recalculate cart */
function recalculateCart()
{
    let subtotal = 0;
    total = 0;
    /* Sum up row totals */
    $('.product').each(function () {
        subtotal += parseFloat($(this).children('.product-line-price').text());
    });

    /* Calculate totals */
    var tax = subtotal * taxRate;
    shipping = ((subtotal > 0 && shippingHome) ? shippingRate*productNumbers : 0);
    total = subtotal + tax + shipping;
    
    /* Update totals display */
    $('.totals-value').fadeOut(fadeTime, function() {
        $('#cart-subtotal').html(subtotal.toFixed(2));
        $('#cart-tax').html(tax.toFixed(2));
        $('#cart-shipping').html(shipping.toFixed(2));
        $('#cart-total').html(total.toFixed(2));
        if(total == 0){
            $('.checkout').fadeOut(fadeTime);
        }else{
            $('.checkout').fadeIn(fadeTime);
        }
        $('.totals-value').fadeIn(fadeTime);
    });
}

/* Update quantity */
function update()
{
    /* Calculate line price */
    $('.product').each(function () {
        var price = parseFloat($(this).children('.product-price').text());
        var quantity = parseFloat($(this).children('.product-quantity').text());
        var linePrice = price * quantity;
         
        productNumbers +=quantity;

        /* Update line price display and recalc cart totals */
        $(this).children('.product-line-price').each(function () {
            $(this).fadeOut(fadeTime, function() {
                $(this).text(linePrice.toFixed(2));
                recalculateCart();
                $(this).fadeIn(fadeTime);
            });
        });
    });
}

/* Remove item from cart */
function removeItem(removeButton)
{
    /* Remove row from DOM and recalc cart total */
    var productRow = $(removeButton).parent().parent();
    var quantity = parseFloat($(productRow).children('.product-quantity').text());
    productNumbers -=quantity;
    productRow.slideUp(fadeTime, function() {
        productRow.remove();
        recalculateCart();
    });

    if(itemQuantity > 0){
        itemQuantity--;
        $('span.badge').css("display", "inherit");
        $('span.badge').html(itemQuantity);
        if(itemQuantity > 9){
            $('span.badge').html('9');
        }
    }
    if(itemQuantity == 0){
        $('span.badge').css("display", "none");
    }
}

function addShipping(){
    if(!shippingHome){
        shippingHome = true;
        shipping = shippingRate * productNumbers
        total += shipping;
        $('#cart-shipping').html(shipping.toFixed(2));
        $('div.totals-item-shipping').slideDown(fadeTime);
        $('#cart-total').html(total.toFixed(2));
        $('.totals-value').fadeIn(fadeTime);
    }
}

function removeShipping(){
    if(shippingHome) {
        shippingHome = false;
        shipping = shippingRate * productNumbers
        total -= shipping;
        $('#cart-shipping').html(shipping.toFixed(2));
        $('div.totals-item-shipping').slideUp(fadeTime);
        $('#cart-total').html(total.toFixed(2));
        $('.totals-value').fadeIn(fadeTime);
    }
}


// handles the click event, sends the query
function getSuccessOutput(urlId) {
    $.ajax({
        url: urlId,
        complete: function (response) {
        },
        error: function () {
            console.log('Errore rilevato!');
        },
    });
    return false;
}

function checkValues(){
    let email = $('div.nohome-name input').attr("value");
    let attrHome = $('div.shipping-home input');
    let attrCard = $('div.payment-type input');

    let i=0;

    if(mode == 1){
        if($("label.mail > input").hasClass("checked")){
            if((email== null || email == "")){
                return false
            }
        }
        else {
            for(i=0;i<attrHome.length; i++){
                if($(attrHome[i]).attr("value") == null || $(attrHome[i]).attr("value") == ""){
                    return false;
                }
            }
        }
    }
    if(mode == 2){
        if($("label.mail > input").hasClass("checked")){
            if((email== null || email == "")){
                return false
            }
        }
        else {
            for(i=0;i<attrHome.length; i++){
                if($(attrHome[i]).attr("value") == null || $(attrHome[i]).attr("value") == ""){
                    return false;
                }
            }
        }
        for(i=0;i<attrCard.length; i++){
            if($(attrCard[i]).attr("value") == null || $(attrCard[i]).attr("value") == ""){
                return false;
            }
        }
    }
    return true;
}

function checkShippingP(attrHome){
    
    return true;
}

$(document).ready(function(){

    $('.product-removal button').click( function() {
        const url = "template/delete-reservation.php?remId=" + $(this).parent().parent().children(".product-details").children(".product-title").attr("value");
        getSuccessOutput(url);
        removeItem(this);
    });

    $('button.checkout').click( function() {
        if(mode == 0) {
            $('div.shopping-cart-container').slideUp(fadeTime);
            $('div.back-to-cart').slideDown(fadeTime);
            $('div.shipping').slideDown(fadeTime);
            $('button.checkout').html("Metodo di pagamento");
            if($("label.home input").hasClass("checked")){
                addShipping();
            }
            mode++;
        }
        else if(mode == 1) {
            if(checkValues()){
                $('div.payment').slideDown(fadeTime);
                $('button.checkout').html("Procedi con l'acquisto");
                $('div.incorrect-parameters').slideUp();
                mode++;
            }
            else {
                $('div.incorrect-parameters').slideDown();
            }
        }
        else if(mode == 2) {
            if(checkValues()){
                window.location = "pagina-carrello.php?payed=1";
            }
            else {
                $('div.incorrect-parameters').slideDown();
            }
        }
    });

    $('div.back-to-cart span').click( function() {
        $('div.incorrect-parameters').slideUp();
        $('div.shipping').slideUp(fadeTime);
        removeShipping();
        if(mode == 2) {
            $('div.payment').slideUp(fadeTime);
        }
        mode = 0;
        $('button.checkout').html("Procedi all'ordine");
        $('div.shopping-cart-container').slideDown(fadeTime);
        $('div.back-to-cart').slideUp(fadeTime);
    });

    $("label.home > input").click( function() {
        $(this).addClass("checked");
        $("label.mail > input").removeClass("checked");
        $('div.shipping-mail').hide();
        $('div.shipping-home').slideDown(fadeTime);
        addShipping();
    });

    $("label.mail > input").click( function() {
        $(this).addClass("checked");
        $("label.home > input").removeClass("checked");
        $('div.shipping-home').slideUp(fadeTime);
        $('div.shipping-mail').slideDown(fadeTime);
        removeShipping();
    });

});


