//numero da far vedere nelle varie categorie di volta in volta
const shows = 3;

function slideDownEvents(table){
    const rows = $(table).children().children();
    let shRows = showedRows(rows);

    if($(table).hasClass("show-all")){
        $(table).parent().children("footer").children().hide();
        for (; shRows < rows.length; shRows++) {
            $(rows[shRows]).addClass("showed");
            $(rows[shRows]).show();
        }
    }
    else {
        let subtotal = 0;
        if((shRows + shows) >= rows.length){
            subtotal = rows.length;
            $(table).parent().children("footer").children().hide();
        }
        else {
            subtotal = shRows + shows;
        }
        
        for (; shRows < subtotal; shRows++) {
            $(rows[shRows]).addClass("showed");
            $(rows[shRows]).show();
        }
    }
}

function nothing(table){
    document.getElementById('main').innerHTML = '<div class="nothing">Nessun evento corrisponde ai parametri di ricerca</div>';
}

function showedRows(rows){
    let i;
    let s = 0;
    for (i = 0; i < rows.length; i++) {
        if($(rows[i]).hasClass("showed")){
            s++
        }
    }
    return s;
}

$(document).ready(function(){

    //faccio comparire gli eventi di ogni categoria pari alla variabile shows, 
    //se una categoria non e' presente in lista-eventi.php non succederà niente.
    slideDownEvents($('table.table-recent'));
    slideDownEvents($('table.table-Concerti'));
    slideDownEvents($('table.table-Teatro'));
    slideDownEvents($('table.table-Cinema'));
    slideDownEvents($('table.table-Sport'));

    $('article > footer > span').click( function() {
        slideDownEvents($(this).parent().parent().children("table"));
    });

    let lista = $("table.lista-eventi");
    if(lista.length == 0){
        nothing();
    }
});