<?php
require_once 'bootstrap.php';

if(!isset($_COOKIE["userId"])){
    $templateParams["titolo"] = "Toway - Login";
    $templateParams["evento"] = "login-form.php";
}
else{
//Base Template
$templateParams["titolo"] = "Toway - Scelta Biglietti";
$templateParams["sceltaposti"] = "sceltaposti.php";
$id = $_GET["id"];
$templateParams["id_evento"] = $id;
$templateParams["date"] = $_GET["date"];
$posti = $dbh->getOccupiedSeats($id, $_GET["date"]);
$piantina = $dbh->getPiantina($id);
if($piantina[0]["piantina"] == 1)
    $templateParams["mappa"] = "sceltaposti-mappa-uno.php";
else if($piantina[0]["piantina"] == 2)
        $templateParams["mappa"] = "sceltaposti-mappa-due.php";
     else
        $templateParams["mappa"] = "sceltaposti-mappa-tre.php";

$templateParams["event"] = $dbh->getEventById($id);
$templateParams["js"] = "posti-mappa.js";
}

require 'template/base.php';
?>