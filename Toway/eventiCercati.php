<?php
require_once 'bootstrap.php';

//Base Template
$templateParams["titolo"] = "Toway - Ricerca";
$templateParams["ricercaAvanzata"] = "ricercaAvanzata.php";
$templateParams["listaeventi"] = "lista-eventi.php";
$city = 0;
$date = 0;
$search = 0;
$Cinema = 0;
$Teatro = 0;
$Concerti = 0;
$Sport = 0;


if(isset($_GET["city"])){
    $city = $_GET["city"];
}
if(isset($_GET["data"])){
    $date = $_GET["data"];
}
if(isset($_GET["search"])){
    $search = $_GET["search"];
}
if(isset($_GET["Cinema"])){
    $Cinema = $_GET["Cinema"];
}
if(isset($_GET["Sport"])){
    $Sport = $_GET["Sport"];
}
if(isset($_GET["Concerti"])){
    $Concerti = $_GET["Concerti"];
}
if(isset($_GET["Teatro"])){
    $Teatro = $_GET["Teatro"];
}
$res = $dbh->advancedSearch($city, $date, $search, $Cinema, $Teatro, $Concerti, $Sport);

$templateParams["cose"] = array("Cinema", "Concerti", "Sport", "Teatro");
$templateParams["Cinema"] = array();
$templateParams["Concerti"] = array();
$templateParams["Sport"] = array();
$templateParams["Teatro"] = array();
$x = 0;

foreach($templateParams["cose"] as $cosa){
    foreach($res as $rec){
        if($rec["data_fine"]>date("Y-m-d")){
            if($rec["categoria"] == $cosa)
                array_push($templateParams[$cosa], $rec);
        }
    }
    if(!isset($templateParams[$cosa][0])){
        unset($templateParams["cose"][$x]);
    }
    $x = $x + 1;
}


$templateParams["js"] = "lista-eventi.js";

$showAll = "show-all";

require 'template/base.php';
?>