<?php
require_once 'bootstrap.php';

//Base Template
$templateParams["titolo"] = "Toway - Home";
$templateParams["bestEvents"] = "bestEvents.php";
$templateParams["ricercaAvanzata"] = "ricercaAvanzata.php";
$templateParams["listaeventi"] = "lista-eventi.php";
$templateParams["recent"] = $dbh->getEventsRecent();
$templateParams["Cinema"] = $dbh->getEventsByCategory("Cinema");
$templateParams["Teatro"] = $dbh->getEventsByCategory("Teatro");
$templateParams["Concerti"] = $dbh->getEventsByCategory("Concerti");
$templateParams["Sport"] = $dbh->getEventsByCategory("Sport");
$templateParams["cose"] = array("recent", "Cinema", "Sport", "Concerti", "Teatro");
$x = 0;
foreach($templateParams["cose"] as $cose){
    if(count($templateParams[$cose])<=0){
        unset($templateParams["cose"][$x]);
    }
    $x = $x + 1;
}
$templateParams["js"] = "lista-eventi.js";
$templateParams["top4"] = $dbh->getTop4();
$templateParams["top4P"] = $dbh->getTop4InPeriod();

require 'template/base.php';
?>