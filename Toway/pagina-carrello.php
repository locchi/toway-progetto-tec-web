<?php
require_once 'bootstrap.php';

if(!isset($_COOKIE["userId"])){
    $templateParams["titolo"] = "Toway - Login";
    header("Refresh:0; url=login.php");
}
else{
if(isset($_GET["id"])){
    $eid = $_GET["id"];
    $date = $_GET["date"];
    $posti = explode(",",$_GET["nposti"]);
    $uid = $_COOKIE["userId"];
    foreach($posti as $posto)
        $dbh->insertReservation($uid, $eid, $date, $posto);
    header("Refresh:0; url=pagina-carrello.php");
}

if(isset($_GET["remId"])){
    $pid = $_GET["remId"];
    $dbh->deleteReservation($pid);
    header("Refresh:0; url=pagina-carrello.php");
}

if(isset($_GET["payed"])){
    $dbh->buyTickets($_COOKIE["userId"]);
    header("Refresh:0; url=login.php");
}

//Base Template
$templateParams["titolo"] = "Toway - Carrello";
$templateParams["evento"] = "carrello.php";
$templateParams["email"] = $dbh->getEmail($_COOKIE["userId"]);
$dbh->deleteReservation(2);
$templateParams["carrello"] = $dbh->getUnacquistedTickets($_COOKIE["userId"]);
$templateParams["js"] = "carrello.js";
}

require 'template/base.php';
?>